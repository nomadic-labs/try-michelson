const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const outputDir = path.join(__dirname, 'build/');
// const MonacoWebpackPlugin = require('monaco-editor-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Dotenv = require('dotenv-webpack')
const webpack = require('webpack')


const isProd = process.env.NODE_ENV === 'production';

module.exports = {
  entry: {
    main: './src/Index.bs.js'},
  mode: isProd ? 'production' : 'development',
  output: {
    path: outputDir,
    filename: 'Index.js'
  },
  module: {
    rules: [{
      test: /\.css$/,
      use: ['style-loader', 'css-loader']
    },
    {
      test: /\.scss$/,
      use: [
        MiniCssExtractPlugin.loader,
        {
          loader: 'css-loader'
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
          }
        }
      ]
    }]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
      inject: false
    }),
    // new MonacoWebpackPlugin({
    //   languages: ['javascript']
    // }),
    new MiniCssExtractPlugin({
      filename: 'css/[name].bundle.css'
    }),
    new Dotenv(),
    new webpack.EnvironmentPlugin([
      "LANG_SERVER_URL"
    ])
  ],
  devServer: {
    compress: true,
    contentBase: outputDir,
    host: "0.0.0.0",
    port: process.env.PORT || 8000,
    historyApiFallback: true
  }
};
