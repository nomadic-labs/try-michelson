[@bs.config {jsx: 3}];

type action =
  | Select(string)
  | Activate
  | Deactivate;

module Elt = {
  type t = {
    id: string,
    effect: unit => unit,
  };

  let repr = (dispatch, active, elt) =>
    if (active === elt.id) {
      <a
        className="dropdown-item is-active"
        onClick={_ => {
          "I've been cliked!"->Js.log
          dispatch(Select(elt.id));
          elt.effect();
        }}>
        elt.id->ReasonReact.string
      </a>;
    } else {
      <a
        className="dropdown-item"
        onClick={_ => {
          "I've been clicked"->Js.log
          dispatch(Select(elt.id));
          elt.effect();
        }}>
        elt.id->ReasonReact.string
      </a>;
    };
};

type state = {
  is_active: bool,
  active_item: string,
  contents: list(Elt.t),
};
let doc = Webapi.Dom.Document.asEventTarget(Webapi.Dom.document);

let handle_click_outside = (dom_elt, e, fn) => {
  open Webapi.Dom;
  let target_elt = MouseEvent.target(e) |> EventTarget.unsafeAsElement;
  !(dom_elt |> Element.contains(target_elt)) ? fn(e) : Js.log("click inside");
};
/* New hook */
let useClickOutside = (onClickOutside: Dom.mouseEvent => unit) => {
  open Webapi.Dom;
  let elt_ref = React.useRef(Js.Nullable.null);
  let handle_mouse_down = e => {
    elt_ref
    ->React.Ref.current
    ->Js.Nullable.toOption
    ->Belt.Option.map(refval =>
        handle_click_outside(refval, e, onClickOutside)
      )
    ->ignore;
  };

  React.useEffect0(() => {
    Document.addMouseDownEventListener(handle_mouse_down, document);
    Some(
      () =>
        Document.removeMouseDownEventListener(handle_mouse_down, document),
    );
  });
  elt_ref;
};

[@react.component]
let make = (~items, ~active_item) => {
  /* Reducer */
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | Select(id) => {...state, active_item: id, is_active: false}
        | Activate => {...state, is_active: true}
        | Deactivate => {...state, is_active: false}
        },
      {is_active: false, active_item, contents: items},
    );

  let div_ref = useClickOutside(_ =>{Js.log("outside!"); dispatch(Deactivate)});

  let className = state.is_active ? "dropdown is-active" : "dropdown";
  <div className ref={div_ref->ReactDOMRe.Ref.domRef}>
    <div className="dropdown-trigger">
      <button
        className="button"
        ariaControls="dropdown-menu"
        onClick={_ => dispatch(Activate)}>
        <span> state.active_item->ReasonReact.string </span>
        <span className="icon is-small">
          <i className="fas fa-angle-down" />
        </span>
      </button>
    </div>
    <div className="dropdown-menu" id="dropdown-menu" role="menu">
      <div className="dropdown-content">
        {state.contents
         |> List.map(Elt.repr(dispatch, state.active_item))
         |> Array.of_list
         |> ReasonReact.array}
      </div>
    </div>
  </div>;
};

module Jsx2 = {
  let component = ReasonReact.statelessComponent("Dropdown");

  let make = (~items, ~active_item) =>
    ReasonReactCompat.wrapReactForReasonReact(
      make,
      makeProps(~items, ~active_item, ()),
    );
};