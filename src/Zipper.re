open Models;
open Location;

type t =
  | ChildLess(Trace.state)
  | Parent(Trace.state, list(t));

type p =
  | Top
  | Node(list(t), p, list(t))
  | ChildNode(Trace.state, list(t), p, list(t));

type loc =
  | Loc(t, p);

let gstate = t =>
  switch (t) {
  | ChildLess(s) => s
  | Parent(s, _) => s
  };

module Intervals = {
  type cmp =
    | LessThan
    | GreaterThan
    | Equal
    | Inside
    | Outside
    | MixedLess
    | MixedGreater;

  let cmp = (a, b) =>
    if (a.stop.point < b.start.point) {
      LessThan;
    } else if (a.start.point > b.stop.point) {
      GreaterThan;
    } else if (a.start.point === b.start.point && a.stop.point === b.stop.point) {
      Equal;
    } else if (a.start.point > b.start.point && a.stop.point <= b.stop.point) {
      Inside;
    } else if (a.start.point < b.start.point && a.stop.point >= b.stop.point) {
      Outside;
    } else if (a.start.point <= b.start.point && a.stop.point <= b.stop.point) {
      MixedLess;
    } else if (a.start.point >= b.start.point && a.stop.point >= b.stop.point) {
      MixedGreater;
    } else {
      failwith("Intervals.cmp unimplemented case");
    };
};

module Printer = {
  let wrap_parens = s => "(" ++ s ++ ")";
  let wrap_brackets = s => "[" ++ s ++ "]";
  open Trace;

  let state = state =>
    Location.Repr.t(state.location)

  let rec tree =
    fun
    | ChildLess(s) => state(s)
    | Parent(s, children) =>
      "P @" ++ state(s) ++ " " ++ trees(children) |> wrap_parens

  and trees = trees =>
    List.map(tree, trees) |> String.concat(",") |> wrap_brackets;

  let rec path =
    fun
    | Top => "top"
    | Node(l, u, r) =>
      "Node(" ++ trees(l) ++ ", " ++ path(u) ++ ", " ++ trees(r) ++ ")"
    | ChildNode(p, l, u, r) =>
      "ChildNode("
      ++ state(p)
      ++ ", "
      ++ trees(l)
      ++ ", "
      ++ path(u)
      ++ ", "
      ++ trees(r)
      ++ ")";

  let loc = (Loc(t, p)) => "t:" ++ tree(t) ++ "\np:" ++ path(p);
};

module Movement = {
  let whatev = "whatev";

  let go_right = (Loc(t, p)) =>
    switch (p) {
    | Top => failwith("top")
    | ChildNode(_, _, _, [])
    | Node(_, _, []) => failwith("right of rightmost")
    | Node(left, up, [r, ...right]) =>
      Loc(r, Node([t, ...left], up, right))
    | ChildNode(pstate, left, up, [r, ...right]) =>
      Loc(r, ChildNode(pstate, [t, ...left], up, right))
    };

  /*
     Parent(x, [child y, child z])
   */

  let go_down = (Loc(t, p)) =>
    switch (t) {
    | ChildLess(_) => failwith("Childless")
    | Parent(state, [eldest, ...siblings]) =>
      Loc(eldest, ChildNode(state, [], p, siblings))
    | Parent(_, []) =>
      failwith("Invariant Violation: father with no children")
    };

  let go_up = (Loc(t, p)) =>
    switch (p) {
    | Top => failwith("Up of top")
    | Node(_) => failwith("This node has no parents")
    | ChildNode(p, left, up, right) =>
      Loc(Parent(p, List.rev(left) @ [t, ...right]), up)
    };

  let go_left = (Loc(t, p)) =>
    switch (p) {
    | Top => failwith("left of top")
    | ChildNode(_, [], _, _)
    | Node([], _, _) => failwith("left of leftmost")
    | Node([l, ...left], up, right) =>
      Loc(l, Node(left, up, [t, ...right]))
    | ChildNode(p, [l, ...left], up, right) =>
      Loc(l, ChildNode(p, left, up, [t, ...right]))
    };

  let insert_right = (Loc(t, p), elt) =>
    switch (p) {
    | Top => failwith("insert_right of top")
    | Node(l, u, r) => Loc(t, Node(l, u, [ChildLess(elt), ...r]))
    | ChildNode(p, l, u, r) =>
      Loc(t, ChildNode(p, l, u, [ChildLess(elt), ...r]))
    };

  let insert_left = (Loc(t, p), elt) =>
    switch (p) {
    | Top => failwith("insert of top")
    | Node(l, u, r) => Loc(t, Node([elt, ...l], u, r))
    | ChildNode(p, l, u, r) => Loc(t, ChildNode(p, [elt, ...l], u, r))
    };

  let insert_down = (Loc(t, p), elt) =>
    switch (t) {
    | ChildLess(_) => failwith("insert down of childless")
    | Parent(state, children) => Loc(elt, ChildNode(state, [], p, children))
    };

  let rec insert_child = (Loc(t, p), elt) =>
    switch (t) {
    | ChildLess(s) => Loc(Parent(s, [ChildLess(elt)]), p)
    | Parent(_) => Loc(t, p)->go_down->insert_elt(elt)
    }

  and insert_inplace = (Loc(t, p), elt) =>
    switch (t) {
    | Parent(_, children) => Loc(Parent(elt, children), p)
    | ChildLess(_) => Loc(ChildLess(elt), p)
    }

  and insert_elt = (Loc(t, p), {location} as elt) => {
    switch (t) {
    | ChildLess({location: treeloc})
    | Parent({location: treeloc}, _) =>
      switch (Intervals.cmp(location->range, treeloc->range)) {
      | GreaterThan =>
        switch (p) {
        | Top => failwith("insert of top")
        | Node(_, _, [])
        | ChildNode(_, _, _, []) => insert_right(Loc(t, p), elt)
        | _ => insert_elt(Loc(t, p)->go_right, elt)
        }
      | Inside => insert_child(Loc(t, p), elt)
      // If the location are the same, that means we're either inside a macro expansion
      // Or inside a loop
      | Equal =>
        switch (location) {
        | Original(_) => insert_right(Loc(t, p), elt)
        | Expanded(_) => Loc(t, p) //insert_inplace(Loc(t, p), elt)
        }

      | s =>
        Js.log(s);
        Js.log(location->Location.Repr.t);
        Js.log(treeloc->Location.Repr.t);
        failwith("whoops what happened");
      }
    };
  };

  let rec go_upmost = (Loc(_, p) as loc) =>
    switch (p) {
    | Top => failwith("movetop of top")
    | ChildNode(_, _, Top, _)
    | Node(_, Top, _) => loc
    | Node(_)
    | ChildNode(_) => loc->go_up->go_upmost
    };

  let rec go_leftmost = (Loc(_, p) as loc) =>
    switch (p) {
    | Top => failwith("moveleftmost of top")
    | Node([], _, _)
    | ChildNode(_, [], _, _) => loc
    | Node(_)
    | ChildNode(_) => loc->go_left->go_leftmost
    };

  let go_topleft = loc => loc->go_upmost->go_leftmost;

  let rec go_next = (Loc(_, p) as loc) =>
    switch (p) {
    | Top => failwith("next of top")
    | Node(_, _, [_, ..._])
    | ChildNode(_, _, _, [_, ..._]) => loc->go_right
    | ChildNode(_, _, _, []) => loc->go_up->go_next
    | Node(_, _, []) => failwith("no right neighbor")
    };

  let go_in = (Loc(t, _) as loc) =>
    switch (t) {
    | Parent(_) => loc->go_down
    | _ => loc->go_next
    };

  let cannot_stepin = (Loc(t, _)) =>
    switch (t) {
    | Parent(_) => false
    | _ => true
    };

  let rec cannot_step = (Loc(_, p) as loc) =>
    switch (p) {
    | Top
    | ChildNode(_, _, _, []) => cannot_step(loc->go_up)
    | Node(_, _, []) => true
    | _ => false
    };

  let cannot_up = (Loc(_, p)) =>
    switch (p) {
    | Top
    | Node(_) => true
    | ChildNode(_) => false
    };
};

module Tree = {
  let get_location = (Loc(t, _)) =>
    switch (t) {
    | ChildLess({location})
    | Parent({location}, _) => location
    };

  let get_state = (Loc(t, _)) =>
    switch (t) {
    | ChildLess(s)
    | Parent(s, _) => s
    };

  let map = (t, f) =>
    switch (t) {
    | ChildLess(_) => f(t)
    | Parent(node, children) =>
      let children = List.map(f, children);
      f(Parent(node, children));
    };

  let map_tree = (Loc(t, p), f) =>
    Loc(map (t, f), p)

  let sort = t =>
    switch (t) {
    | ChildLess(_) => t
    | Parent(node, children) =>
      let children =
        List.sort(
          (a, b) => compare(b->gstate.gas, a->gstate.gas),
          children,
        );
      Parent(node, children)
    };

  let rec previous = (Loc(_, p) as loc) =>
    switch (p) {
    | Top => failwith("previous of top")
    | Node([], _, _) => None
    | Node([_, ..._], _, _)
    | ChildNode(_, [_, ..._], _, _) =>
      Some(loc->Movement.go_left->get_state)
    | ChildNode(_, [], _, _) => previous(loc->Movement.go_up)
    };

  let build = l => {
    let l =
      Trace.sort(l)
      |> List.filter((s: Trace.state) => s.location->range.start.point != 0)
      |> List.filter((s: Trace.state) => !(s.location->Location.is_expanded ))

    let rec build_tree = (tree, elts) => {
      let tree = tree->Movement.go_topleft;
      switch (elts) {
      | [] => tree
      // Ignore non real locations
      | [x, ...l] when Location.Lens.start_point(x.Trace.location) === 0 =>
        Js.log("Unexisting location");
        build_tree(tree, l);
      | [x, ...l] => tree->Movement.insert_elt(x)->build_tree(l)
      };
    };

    switch (l) {
    | [] => failwith("can't build tree from empty list")
    | [x, ...l] =>
      Loc(ChildLess(x), Node([], Top, []))
      ->build_tree(l)
      ->(
          tree => {
            Printer.loc(tree)->Js.log;
            tree;
          }
        )
      ->Movement.go_leftmost
      ->map_tree(sort)
    };
  };
};