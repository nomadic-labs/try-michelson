open Models;
open Utils;
open PromiseMonad;

let delta_key = () => Js.Float.toString(Random.float(12931298391283981.0));

type movement =
  | Next
  | NextIn
  | NextUp;

module Exec_result = {
  type t = {
    trace: Trace.t,
    operations: list(Transaction.t),
    expanded: string,
    gas_left: int,
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      trace: json |> field("trace", Trace.Decode.t),
      operations: json |> field("operations", list(Transaction.Decode.t)),
      expanded: json |> field("expanded", string),
      gas_left: json |> field("gas_left", string) |> int_of_string,
    };
  };
};

type trace_state = {
  zip: Zipper.loc,
  active_contract_id: int,
  max_stack_size: int,
};

/* In fact, editors should handle their errors */
type edition_state = {
  last_edited_id: int,
  error: option(string),
};

type travel_state = {
  snapshot_index: int,
  history: History.t,
  current_snapshot: Snapshot.t,
  trace_state: option(trace_state),
  show_trace: bool,
};

type mode =
  | Traveling(travel_state)
  | Edition(edition_state);

type state = {
  active_ids: list(int),
  contracts: list((int, Contract.t)),
  id_exists: ref(int => bool),
  mode,
  server_connected: bool,
  params: Presets.params,
  delta_key: string /* This is used to know when to reload form */
};

type params = {
  amount: int,
  parameter: string,
  gas_limit: int,
  id: int,
  protocol: Protocol.t,
  payer: string,
  entrypoint: string,
  balances: list(int),
};

type action =
  /* Server connections */
  | Ping(int)
  | UpdateConnection(bool)
  /* Change state actions */
  | FetchTrace(params)
  | GlobalError(string)
  /* Tracing actions */
  | Move(movement)
  /* Contracts call. first parameter is id. */
  | FetchTypemap(int /* id */)
  /* We have two timer actions, 'cause we want to side-effect, then update state */
  | SetTimer(int)
  | AddTimer(int, Js.Global.timeoutId)
  | UpdateTypemap(int, Typemap.t)
  | UpdateErrors(int, option(Error.typecheck_error))
  | UpdateText(int, string)
  | UpdateStorage(int, string)
  | UpdateBalance(int, string)
  | UpdateTypechecks(int, bool)
  | UpdateEntrypoints(int, list(Entrypoint.t))
  | ShowTypestack(int, int, int)
  | GenStorage(int, string)
  /* history actions */
  | StopTravel
  | LoadHistory(History.t)
  | LoadReceipt(Receipt.t)
  | NextOp
  | PreviousOp
  | ToggleTrace
  /* Contract creation & deletion */
  | CreateContract(Presets.contract_content)
  | DeleteContract(int)
  | ReplaceContracts(list(Presets.contract_content))
  /* Parameters */
  | UpdateParams(Presets.params)
  /* Load preset scenario */
  | LoadScenario(Presets.scenario);

module Contracts = {
  open Contract;
  open ReasonReact;

  let get_contract = (id, state) => List.assoc(id, state.contracts);

  let get_editor = (id, state) => List.assoc(id, state.contracts).editor;
  let get_text = (id, state) => List.assoc(id, state.contracts).text;
  let get_storage = (id, state) => List.assoc(id, state.contracts).storage;
  let get_errors = (id, state) => List.assoc(id, state.contracts).errors;

  let get_timers = (id, state) => List.assoc(id, state.contracts).timers;

  let get_typemap = (id, state) => List.assoc(id, state.contracts).typemap;

  let get_balance = (id, state) =>
    switch (List.assoc(id, state.contracts).balance |> int_of_string) {
    | balance => balance
    | exception (Failure(_)) => 0
    };

  let make_callbacks = (id, self) => {
    update_text: text => self.send(UpdateText(id, text)),
    update_storage: text => self.send(UpdateStorage(id, text)),
    update_balance: balance => self.send(UpdateBalance(id, balance)),
    load_editor: monaco =>
      Contract.load_editor(get_contract(id, self.state), monaco),
    load_decorate: f =>
      Contract.load_decorate(get_contract(id, self.state), f),
    delete_me: () => self.send(DeleteContract(id)),
    update_typemap:
      (. line, col) => self.send(ShowTypestack(id, line, col)),
    gen_storage: text => self.send(GenStorage(id, text)),
  };

  let map_single = (t, update_id, f) => {
    List.map(
      ((id, contract)) =>
        if (id === update_id) {
          (id, f(contract));
        } else {
          (id, contract);
        },
      t,
    );
  };

  let map = (t, f) => List.map(((id, contract)) => (id, f(contract)), t);

  let remove_errors = t => map(t, contract => {...contract, errors: None});

  let find = (t, addr) => List.find(((_, c)) => c.address === addr, t);
};

module Presets = {
  include Presets;
  // open Dropdown.Elt;

  let to_item = (contract_id, self, preset: contract) => {
    id: preset.name,
    Dropdown.Elt.effect: () => {
      self.ReasonReact.send(UpdateText(contract_id, preset.content.code));
      self.send(UpdateStorage(contract_id, preset.content.storage));
    },
  };

  let item_of_scenario = (self, scenario: scenario) => {
    id: scenario.name,
    Dropdown.Elt.effect: () => {
      self.ReasonReact.send(LoadScenario(scenario));
    },
  };
};

let component = ReasonReact.reducerComponent("TryComponent");
let make = _children => {
  ...component,

  didMount: self => {
    let code = Contracts.get_text(0, self.state);
    ignore(
      Request.Request.indent(~code)
      >>- Json.Decode.string
      >>- (str => self.send(UpdateText(0, str))),
    );
    self.send(GenStorage(0, code));
    self.send(FetchTypemap(0));
  },

  initialState: () => {
    let default_code = "parameter unit;
storage unit;
code { CAR; NIL operation; PAIR }";
    let params = Url.params;
    let params =
      if (Url.has("source", params) || Url.url == "") {
        params;
      } else {
        LzString.decompressFromEncodedUriComponent(Url.to_string(params))
        |> Url.get_params;
      };
    let code = Url.get_with_default("source", default_code, params);

    let storage = Url.get_with_default("storage", "", params);
    {
      contracts: [
        (
          0,
          Contract.make(code, storage, "0", Genesis.contract_addresses[0]),
        ),
      ],
      active_ids: [0],
      id_exists: ref(id => id === 0),
      mode: Edition({last_edited_id: 0, error: None}),
      server_connected: true,
      params: Presets.default_params,
      delta_key: "original",
    };
  },

  reducer: (action, state) => {
    Zipper.(
      switch (state.mode, action) {
      | (_, Ping(duration)) =>
        ReasonReact.SideEffects(
          self =>
            ignore(
              Request.Request.hello()
              >>- (_ => self.send(UpdateConnection(true)))
              >>| (
                _ => {
                  self.send(UpdateConnection(false));
                  let _ =
                    Js.Global.setTimeout(
                      () => self.send(Ping(duration * 2)),
                      duration,
                    );
                  return();
                }
              ),
            ),
        )
      | (_, UpdateConnection(server_connected)) =>
        ReasonReact.Update({...state, server_connected})
      | (_, LoadHistory(history)) =>
        let current_snapshot = List.hd(history);
        ReasonReact.UpdateWithSideEffects(
          {
            ...state,
            mode:
              Traveling({
                snapshot_index: 0,
                current_snapshot,
                history,
                trace_state: None,
                show_trace: false,
              }),
          },
          self => self.send(LoadReceipt(current_snapshot.receipt)),
        );
      | (
          Traveling(
            {trace_state: Some({zip, active_contract_id} as tstate)} as travel_state,
          ),
          Move(dir),
        ) =>
        let zip =
          switch (dir) {
          | Next => zip->Movement.go_next
          | NextIn => zip->Movement.go_in
          | NextUp => zip->Movement.go_up
          };
        // let location = Tree.get_location(zip);
        ReasonReact.Update({
          ...state,
          mode:
            Traveling({
              ...travel_state,
              trace_state: Some({...tstate, zip, active_contract_id}),
            }),
        });
      | (Traveling(travel_state), LoadReceipt(receipt)) =>
        switch (receipt.op) {
        | Transaction({destination}) =>
          switch (receipt.result->OperationResult.get_trace_opt) {
          | None => ReasonReact.NoUpdate
          | Some(trace) =>
            let max_stack_size = Models.Trace.max_stack_size(trace);
            let zip = Tree.build(trace);
            let (active_contract_id, _) =
              Contracts.find(state.contracts, destination);
            let trace_state = Some({zip, active_contract_id, max_stack_size});
            ReasonReact.Update({
              ...state,
              mode: Traveling({...travel_state, trace_state}),
            });
          }
        | Origination(_) => ReasonReact.NoUpdate
        | Delegation(_) => ReasonReact.NoUpdate
        }
      | (Traveling({history, snapshot_index, _} as travel_state), NextOp) =>
        if (snapshot_index >= List.length(history)) {
          Js.log("tried to access further than history...");
          ReasonReact.NoUpdate;
        } else {
          let snapshot_index = snapshot_index + 1;
          let current_snapshot = List.nth(history, snapshot_index);
          Js.log(current_snapshot->Snapshot.Repr.dump_string);
          ReasonReact.UpdateWithSideEffects(
            {
              ...state,
              mode:
                Traveling({
                  ...travel_state,
                  trace_state: None,
                  current_snapshot,
                  snapshot_index,
                }),
            },
            self => {
              List.iter(
                ((_, c)) => Contract.undecorate(c),
                self.state.contracts,
              );
              self.send(LoadReceipt(current_snapshot.receipt));
            },
          );
        }
      | (Traveling({history, snapshot_index} as travel_state), PreviousOp) =>
        switch (snapshot_index) {
        | 0 => ReasonReact.NoUpdate
        | _ =>
          let snapshot_index = snapshot_index - 1;
          let current_snapshot = List.nth(history, snapshot_index);
          ReasonReact.UpdateWithSideEffects(
            {
              ...state,
              mode:
                Traveling({
                  ...travel_state,
                  current_snapshot,
                  snapshot_index,
                }),
            },
            self => {
              List.iter(
                ((_, c)) => Contract.undecorate(c),
                self.state.contracts,
              );
              self.send(LoadReceipt(current_snapshot.receipt));
            },
          );
        }
      | (Traveling({show_trace, trace_state} as t), ToggleTrace) =>
        let show_trace = !show_trace;
        switch (trace_state, show_trace) {
        | (Some({active_contract_id}), false) =>
          Contract.undecorate(
            Contracts.get_contract(active_contract_id, state),
          )
        | _ => ()
        };
        ReasonReact.Update({...state, mode: Traveling({...t, show_trace})});
      | (_, FetchTrace(params)) =>
        let id = params.id;
        let code = Contracts.get_contract(id, state).text;
        let storage = Contracts.get_contract(id, state).storage;
        let address = Contracts.get_contract(id, state).address;
        let balance = Contracts.get_balance(id, state);
        let contracts =
          List.map(
            ((_, contract)) =>
              (
                contract.Contract.address,
                contract.Contract.text,
                contract.storage,
                switch (int_of_string(contract.balance)) {
                | b => b
                | exception (Failure(_)) =>
                  Js.log("[WARN] balance_failure");
                  0;
                },
              ),
            state.contracts,
          );
        ReasonReact.SideEffects(
          self =>
            ignore(
              Request.Request.get_trace(
                ~code,
                ~storage,
                ~balance,
                ~self=address,
                ~parameter=params.parameter,
                ~gas_limit=params.gas_limit,
                ~amount=params.amount,
                ~payer=params.payer,
                ~source=params.payer,
                ~boot_balances=params.balances,
                ~entrypoint=params.entrypoint,
                ~contracts,
                ~timestamp="2018-03-26T13:37:00Z",
                ~protocol=params.protocol,
              )
              >>- Json.Decode.oneOf([
                    History.Decode.t
                    |> Json.Decode.map(history =>
                         self.send(LoadHistory(history))
                       ),
                    Error.Decode.typecheck_error
                    |> Json.Decode.map(err =>
                         self.send(UpdateErrors(id, Some(err)))
                       ),
                  ])
              >>| (
                err => {
                  Js.log(err);
                  self.send(Ping(1000));
                  return();
                }
              ),
            ),
        );
      | (_, FetchTypemap(id)) =>
        let code = Contracts.get_text(id, state);
        let storage = Contracts.get_storage(id, state);
        ReasonReact.SideEffects(
          self =>
            ignore(
              Request.Request.typecheck(
                ~code,
                ~storage,
                ~protocol=state.params.protocol,
              )
              >>- Json_decode.oneOf([
                    TypecheckResponse.Decode.t
                    |> Json.Decode.map((response: TypecheckResponse.t) => {
                         self.send(
                           UpdateTypemap(
                             id,
                             response.typemap |> Models.Typemap.filter_expanded,
                           ),
                         );
                         self.send(UpdateTypechecks(id, true));
                         self.send(UpdateErrors(id, None));
                         self.send(
                           UpdateEntrypoints(id, response.entrypoints),
                         );
                       }),
                    Error.Decode.typecheck_error
                    |> Json.Decode.map(err => {
                         self.send(
                           UpdateTypemap(
                             id,
                             err.Error.typemap
                             |> Models.Typemap.filter_expanded,
                           ),
                         );
                         self.send(UpdateTypechecks(id, false));
                         self.send(UpdateErrors(id, Some(err)));
                       }),
                  ])
              >>| (
                err => {
                  Js.log(err);
                  self.send(UpdateTypemap(id, [])); //Reset typemap
                  self.send(UpdateTypechecks(id, false));
                  self.send(Ping(1000));
                  return();
                }
              ),
            ),
        );
      | (_, Move(_)) => ReasonReact.NoUpdate
      | (Traveling({trace_state}), StopTravel) =>
        ReasonReact.UpdateWithSideEffects(
          {...state, mode: Edition({last_edited_id: 0, error: None})},
          _self =>
            switch (trace_state) {
            | None => ()
            | Some({active_contract_id}) =>
              Contracts.get_contract(active_contract_id, state)
              ->Contract.undecorate
            },
        )
      | (_, StopTravel) => ReasonReact.NoUpdate
      | (Edition(st), GlobalError(err)) =>
        ReasonReact.Update({
          ...state,
          mode: Edition({...st, error: Some(err)}),
        })
      | (_, AddTimer(contract_id, timer_id)) =>
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, Contract.timers: [timer_id]}
          );
        ReasonReact.Update({...state, contracts});
      | (_, SetTimer(id)) =>
        let timers = Contracts.get_timers(id, state);
        ReasonReact.SideEffects(
          self => {
            List.iter(timer_id => Js.Global.clearTimeout(timer_id), timers);
            let timer_id =
              Js.Global.setTimeout(() => self.send(FetchTypemap(id)), 250);
            self.send(AddTimer(id, timer_id));
          },
        );
      | (_, UpdateTypemap(contract_id, typemap)) =>
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, Contract.typemap}
          );
        ReasonReact.Update({...state, contracts});
      | (_, UpdateErrors(contract_id, errors)) =>
        switch (errors) {
        | None =>
          Contract.undecorate(Contracts.get_contract(contract_id, state))
        | Some(err) =>
          List.map(Error.get_loc_opt, err.details)
          |> Utils.List.filter_opt
          |> List.iter(loc =>
               Contract.decorate(
                 Contracts.get_contract(contract_id, state),
                 "error",
                 loc,
               )
             )
        };
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, Contract.errors}
          );
        ReasonReact.Update({...state, contracts});
      | (_, UpdateBalance(contract_id, balance)) =>
        Js.log(contract_id);
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, balance}
          );
        ReasonReact.Update({...state, contracts});
      | (_, ShowTypestack(contract_id, line, col)) =>
        let typetrans =
          Contracts.get_typemap(contract_id, state) |> Typemap.at(line, col);
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, Contract.shown_typetrans: typetrans}
          );
        ReasonReact.Update({...state, contracts});
      | (_, GenStorage(update_id, code)) =>
        ReasonReact.SideEffects(
          self => {
            Js.log("genning");
            ignore(
              Request.Request.gen_param_storage(~code)
              >>- Request.Decode.param_storage
              >>- (t => self.send(UpdateStorage(update_id, snd(t)))),
            );
          },
        )
      | (_, UpdateStorage(update_id, storage)) =>
        let contracts =
          Contracts.map_single(state.contracts, update_id, contract =>
            {...contract, Contract.storage}
          );
        ReasonReact.UpdateWithSideEffects(
          {...state, contracts},
          self => self.send(FetchTypemap(update_id)),
        );
      | (_, UpdateTypechecks(contract_id, typechecks)) =>
        let contracts =
          Contracts.map_single(state.contracts, contract_id, contract =>
            {...contract, Contract.typechecks}
          );
        ReasonReact.Update({...state, contracts});
      | (_, UpdateText(update_id, text)) =>
        let contracts =
          Contracts.map_single(state.contracts, update_id, contract =>
            {...contract, Contract.text}
          );
        ReasonReact.UpdateWithSideEffects(
          {...state, contracts},
          self => {
            self.send(SetTimer(update_id));
            self.send(StopTravel);
          },
        );
      | (_, UpdateEntrypoints(update_id, entrypoints)) =>
        let names = List.map(entry => entry.Entrypoint.name, entrypoints);
        let entrypoints = ["default", ...names];
        let contracts =
          Contracts.map_single(state.contracts, update_id, contract =>
            {...contract, Contract.entrypoints}
          );
        ReasonReact.Update({...state, contracts, delta_key: delta_key()});
      | (_, DeleteContract(delete_id)) =>
        // Don't allow to delete if there's only one contract left.
        switch (List.length(state.contracts)) {
        | 1 => ReasonReact.NoUpdate
        | _ =>
          /* If an editor is deleted, and there's some after it,
             we shift them. This is not pretty, but that's the only
             hack we have to find the correct model to highlight in
             monaco. */
          let contracts =
            state.contracts
            |> List.map(((id, contract: Contract.t)) =>
                 switch (compare(id, delete_id)) {
                 | 0 => None
                 | x when x < 0 => Some((id, contract))
                 | _ =>
                   let id = id - 1;
                   let address = Genesis.contract_addresses[id];
                   Some((id, {...contract, address}));
                 }
               )
            |> Utils.List.filter_opt;
          let active_ids =
            state.active_ids
            |> List.map(id =>
                 switch (compare(id, delete_id)) {
                 | 0 => None
                 | x when x < 0 => Some(id)
                 | _ => Some(id - 1)
                 }
               )
            |> Utils.List.filter_opt;
          ReasonReact.Update({...state, contracts, active_ids});
        }
      | (_, CreateContract({code, storage})) =>
        // active_ids is sorted, so the last element is the highest id
        let id =
          List.nth(state.active_ids, List.length(state.active_ids) - 1) + 1;
        let address = Genesis.contract_addresses[id];
        let contract = Contract.make(code, storage, "0", address);
        /* The insert order is important here */
        let active_ids = state.active_ids @ [id];
        let contracts = state.contracts @ [(id, contract)];
        ReasonReact.UpdateWithSideEffects(
          {...state, active_ids, contracts, delta_key: delta_key()},
          self =>
            self.state.id_exists := ((id: int) => List.mem(id, active_ids)),
        );
      | (Edition(_), UpdateParams(params)) =>
        //FIXME
        let delta_key = delta_key();
        ReasonReact.Update({...state, params, delta_key});
      | (Edition(_), LoadScenario(scenario)) =>
        ReasonReact.SideEffects(
          self => {
            List.iter(
              id => self.send(DeleteContract(id)),
              self.state.active_ids,
            );
            scenario.contracts
            |> List.iter(contr => self.send(CreateContract(contr)));
            self.send(DeleteContract(0));
            self.send(UpdateParams(scenario.params));
            scenario.contracts
            |> List.iteri((id, _) => self.send(FetchTypemap(id)));
          },
        )
      | (state, action) =>
        Js.log(state);
        Js.log(action);
        assert(false);
        ReasonReact.NoUpdate;
      }
    );
  },

  render: self => {
    Js.log("rerender!");
    <div>
      {switch (self.state.mode) {
       | Traveling(_) => ReasonReact.null
       | Edition({error}) =>
         switch (error) {
         | None => ReasonReact.null
         | Some(err) =>
           <div className="section container">
             <div className="notification is-danger">
               err->ReasonReact.string
             </div>
           </div>
         }
       }}
      {self.state.server_connected
         ? ReasonReact.null
         : <div className="section container">
             <div className="notification is-danger">
               "server unreachable"->ReasonReact.string
             </div>
           </div>}
      <div className="container">
        // <Dropdown
        //   items={List.map(Presets.item_of_scenario(self), Presets.scenarios)}
        //   active_item="Load scenario"
        // />

          {switch (self.state.mode) {
           | Traveling({current_snapshot, snapshot_index, history}) =>
             <div>
               <button
                 className="button"
                 onClick={_ => self.send(NextOp)}
                 disabled={snapshot_index >= List.length(history) - 1}>
                 "Next operation"->React.string
               </button>
               <button
                 className="button"
                 onClick={_ => self.send(PreviousOp)}
                 disabled={snapshot_index === 0}>
                 "Previous operation"->React.string
               </button>
               <button
                 className="button" onClick={_ => self.send(StopTravel)}>
                 "Stop travel"->ReasonReact.string
               </button>
               <p> "operations:"->React.string </p>
               <ul>
                 {List.map(snap => snap.Snapshot.receipt, history)
                  |> List.map(receipt =>
                       <li>
                         {receipt->Receipt.Repr.to_html(
                            () => self.send(ToggleTrace),
                            (addr, storage) => {
                              let id =
                                Contracts.find(self.state.contracts, addr)
                                ->fst;
                              self.send(UpdateStorage(id, storage));
                            },
                          )}
                       </li>
                     )
                  |> Array.of_list
                  |> ReasonReact.array}
               </ul>
             </div>
           | _ =>
             <div className="container">
               <Dropcard.Jsx2
                 key={self.state.delta_key}
                 hidden={
                   <ParamsForm.Jsx2
                     submit={(
                       parameter,
                       gas_limit,
                       amount,
                       id,
                       payer,
                       entrypoint,
                       balances,
                     ) => {
                       self.send(
                         UpdateParams({
                           parameter,
                           gas: gas_limit->string_of_int,
                           amount: amount->string_of_int,
                           id: id->string_of_int,
                           payer,
                           entrypoint,
                           boot_balances:
                             List.map(string_of_int, balances)->Array.of_list,
                           protocol: self.state.params.protocol,
                         }),
                       );
                       self.send(
                         FetchTrace({
                           parameter,
                           gas_limit,
                           amount,
                           id,
                           payer,
                           entrypoint,
                           balances,
                           protocol: self.state.params.protocol,
                         }),
                       );
                     }}
                     id_exists={self.state.id_exists}
                     parameter={self.state.params.parameter}
                     gas_limit={self.state.params.gas}
                     amount={self.state.params.amount}
                     id={self.state.params.id}
                     payer={self.state.params.payer}
                     balances={self.state.params.boot_balances}
                     key={self.state.delta_key}
                     server_connected={self.state.server_connected}
                     contracts={self.state.contracts}
                   />
                 }
                 content={
                   <p className="card-header-title">
                     "Run transaction"->ReasonReact.string
                   </p>
                 }
               />
             </div>
           }}
          <div className="section">
            <button
              className="button"
              disabled={List.length(self.state.active_ids) === 5}
              onClick={_ =>
                self.send(CreateContract(Presets.default_contract))
              }>
              "New contract"->ReasonReact.string
            </button>
            {self.state.active_ids
             |> List.map(id =>
                  <div>
                    {switch (self.state.mode) {
                     | Traveling({
                         trace_state:
                           Some({zip, active_contract_id, max_stack_size}),
                         show_trace,
                       })
                         when active_contract_id === id && show_trace =>
                       let state = Zipper.Tree.get_state(zip);
                       let stack = state.stack;
                       let location = Zipper.Tree.get_location(zip);
                       Contracts.get_contract(active_contract_id, self.state)
                       ->Contract.decorate("highlight-trace", location);
                       let prev_stack =
                         switch (Zipper.Tree.previous(zip)) {
                         | None => ReasonReact.null
                         | Some(state) =>
                           <StackComponent
                             stack={state.stack}
                             max_stack_size
                           />
                         };
                       <div className="section">
                         <div className="container">
                           <button
                             className="button"
                             onClick={_ => self.send(Move(Next))}
                             disabled={Zipper.Movement.cannot_step(zip)}>
                             {ReasonReact.string("Step")}
                           </button>
                           <button
                             className="button"
                             onClick={_ => self.send(Move(NextIn))}
                             disabled={Zipper.Movement.cannot_stepin(zip)}>
                             {ReasonReact.string("Step in")}
                           </button>
                           <button
                             className="button"
                             onClick={_ => self.send(Move(NextUp))}
                             disabled={Zipper.Movement.cannot_up(zip)}>
                             {ReasonReact.string("Up")}
                           </button>
                           <button
                             className="button"
                             onClick={_ => self.send(ToggleTrace)}>
                             "Stop trace"->React.string
                           </button>
                         </div>
                         <div className="container" id="stacks">
                           <div className="columns">
                             <div className="column is-5"> prev_stack </div>
                             <div className="column is-2 offset-5">
                               <i className="fas fa-gas-pump">
                                 {state.gas->string_of_int->ReasonReact.string}
                               </i>
                             </div>
                             <div className="column is-5 offset-7">
                               <StackComponent stack max_stack_size />
                             </div>
                           </div>
                         </div>
                       </div>;
                     | _ => ReasonReact.null
                     }}
                    <ContractComponent
                      is_tracing={
                        (
                          fun
                          | Traveling(_) => true
                          | _ => false
                        )(
                          self.state.mode,
                        )
                      }
                      id
                      items={List.map(
                        Presets.to_item(id, self),
                        Presets.contracts,
                      )}
                      server_connected={self.state.server_connected}
                      is_alone={List.length(self.state.active_ids) === 1}
                      state={Contracts.get_contract(id, self.state)}
                      callbacks={Contracts.make_callbacks(id, self)}
                      key={string_of_int(id)}
                    />
                  </div>
                )
             |> Array.of_list
             |> ReasonReact.array}
          </div>
        </div>
    </div>;
  },
};
