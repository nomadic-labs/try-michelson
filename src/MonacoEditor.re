[@bs.obj]
external makeProps: (
  ~language: string=?,
  ~height: int=?,
  ~width: string=?,
  ~theme: string=?,
  // unit should be an event
  ~onChange: (string, unit) => unit=?,
  ~editorDidMount: ((Monaco.editor, Monaco.monaco) => unit)=?,
  ~options: (Js.t(_))=?,
  ~value: string=?,
  ~ref: unit=?,
  unit
) => _ = "";

[@bs.module "react-monaco-editor"] external jsMonacoEditor
  : ReasonReact.reactClass = "default";

let make = (
  ~language=?,
  ~height=?,
  ~width=?,
  ~onChange=?,
  ~editorDidMount=?,
  ~theme=?,
  ~options=?,
  ~value=?,
  ~ref=?,
  children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass = jsMonacoEditor,
    ~props = makeProps(
      ~language?,
      ~height?,
      ~width?,
      ~onChange?,
      ~editorDidMount?,
      ~theme?,
      ~options?,
      ~value?,
      ~ref?,
      ()),
    children);
