open Models

[@bs.module]
external json: Js.Json.t = "./presets.json"

type contract_content = {
  code: string,
  storage: string,
  balance: int
};

type contract = {
  name: string,
  content: contract_content
};

type params = {
  parameter: string,
  gas: string,
  amount: string,
  id: string,
  payer: string,
  entrypoint: string,
  protocol: Protocol.t,
  boot_balances: array(string)
};

type scenario = {
  name: string,
  contracts: list(contract_content),
  params,
};

type bootstraps = {
  name: string,
  balances: array(string)
}

module Decode = {
  open Json.Decode;

  let contract_content = json => {
    code: json |> field("code", string),
    storage: json |> field("storage", string),
    balance: json |> field("balance", int)
  }


  let contract = json => {
      name: json |> field("name", string),
      content: json |> contract_content
  }

  let contract_presets = json => list(contract, json)

  let load_from = (contracts, json) => {
    let id = field("load_from", string, json)
    List.find(({name} : contract) => id === name, contracts).content
  }

  let scenario_contract = (contracts, json) => json |> oneOf([
    contract_content,
    load_from(contracts)
  ])

  let scenario_contracts = (contracts, json) => list(scenario_contract(contracts), json)

  let bootstraps = json => {
    name: json |> field("name", string),
    balances: json |> field("balances", array(string))
  }

  let bootstraps_list = json => field("bootstraps", list(bootstraps), json)

  let load_from_bootstrap = (bootstraps: list(bootstraps), json) => {
    let id = string(json);
    List.find(({name}: bootstraps) => id === name, bootstraps).balances
  }

  let boot_balances = (bootstraps, json) =>
    json |> oneOf([
      array(string),
      load_from_bootstrap(bootstraps)
    ])

  let params = (bootstraps, json) => {
    parameter: json |> field("parameter", string),
    gas: json |> field("gas", string),
    amount: json |> field("mutez", string),
    id: json |> field("id", string),
    payer: json |> field("payer", string),
    entrypoint: json |> field("entrypoint", string),
    boot_balances: json |> field("boot_balances", boot_balances(bootstraps)),
    protocol: json |> field("protocol", Protocol.Decode.t)
  }

  let scenario = (bootstraps, contracts, json) => {
    name: json |> field("name", string),
    contracts: json |> field("contracts", scenario_contracts(contracts)),
    params: json |> field("params", params(bootstraps))
  }

  
  let scenarios = (bootstraps, contracts, json) => list(scenario(bootstraps, contracts), json)

  // let presets = json => {
  //   let contracts = field("contract_presets", contract_presets, json)
  //   let scenarios: list(scenario) = field("scenarios", scenarios(contracts), json)
  //   (contracts, scenarios)
  // }

  let contracts = json => field("contract_presets", contract_presets, json)

  let scenarios = (bootstraps, contracts, json) => field("scenarios", scenarios(bootstraps, contracts), json)
}
let contracts = Decode.contracts(json)
Js.log("contracts")
let bootstraps = Decode.bootstraps_list(json)
Js.log("bootstraps")
let scenarios = Decode.scenarios(bootstraps, contracts, json)
Js.log("scenarios")


let default_contract = List.find(({name} : contract) => name === "if", contracts).content
let default_params = List.hd(scenarios).params