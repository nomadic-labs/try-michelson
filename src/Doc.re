[@bs.module]
external json: Js.Json.t = "./suggestions.json";

type prim = {
  label: string,
  doc: string,
  detail: string,
  kind: string,
  semantics: list(string),
  insert: string
};

module Decode = {
  open Json.Decode;

  let prim = json => {
    label: json |> field("label", string),
    doc: json |> field("documentation", string),
    detail: json |> field("detail", string),
    kind: json |> field("kind", string),
    semantics: json |> field("semantics", list(string)),
    insert: json |> field("insertText", string)
  }

  let prims = json => list(prim, json)
}

let prims = Decode.prims(json)