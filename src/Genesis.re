let bootstraps = [|
"tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx",
"tz1gjaF81ZRRvdzjobyfVNsAeSC6PScjfQwN",
"tz1faswCTDciRzE4oJ9jn2Vm2dvjeyA9fUzU",
"tz1b7tUupMgCNw2cCLpKTkSD1NZzB5TkP2sv",
"tz1ddb9NMYHZi5UzPdzTZMYQQZoMub195zgv"
|]

let contract_addresses = [|
  "KT1QuofAgnsWffHzLA7D78rxytJruGHDe7XG",
  "KT1CSKPf2jeLpMmrgKquN2bCjBTkAcAdRVDy",
  "KT1SLWhfqPtQq7f4zLomh8BNgDeprF9B6d2M",
  "KT1WPEis2WhAc2FciM2tZVn8qe6pCBe9HkDp",
  "KT1Um7ieBEytZtumecLqGeL56iY6BuWoBgio",
  "KT1Cz7TyVFvHxXpxLS57RFePrhTGisUpPhvD",
  "KT1Q1kfbvzteafLvnGz92DGvkdypXfTGfEA3",
  "KT1PDAELuX7CypUHinUgFgGFskKs7ytwh5Vw",
  "KT1A56dh8ivKNvLiLVkjYPyudmnY2Ti5Sba3",
  "KT1RUT25eGgo9KKWXfLhj1xYjghAY1iZ2don",
|];

let balances = [0, 0, 0, 0, 0]