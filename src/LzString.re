type lzstring;

[@bs.module "lz-string"]
external compressToEncodedUriComponent: string => string =
  "compressToEncodedURIComponent";

[@bs.module "lz-string"]
external decompressFromEncodedUriComponent: string => string =
  "decompressFromEncodedURIComponent";