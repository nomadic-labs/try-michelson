open PromiseMonad;
open Models.Contract;
[@bs.module "./monacot"]
external get_value: Monaco.editor => string = "get_value";
[@bs.module "./monacot"]
external on_cursor_move: (Monaco.editor, (. int, int) => unit) => unit =
  "onCursorMove";
[@bs.module "./monacot"]
external setDecoration:
  (Monaco.editor, Monaco.monaco) => (. string, (int, int, int, int)) => unit =
  "setDecoration";
[@bs.module "./monacot"]
external set_syntax_highlighting:
  Monaco.monaco => unit = "setSyntaxHighlighting";

[@bs.module "./monacot"]
external set_hover:
  (
    Monaco.editor,
    Monaco.monaco,
    (. string, string, int, int, int, int) => string
  ) =>
  unit =
  "setHover";
[@bs.module "./monacot"]
external set_completion: Monaco.monaco => unit = "setCompletion";

let init_monaco = ref(true);

let component = ReasonReact.statelessComponent("Contract Component");
let make =
    (
      ~is_tracing,
      ~is_alone,
      ~id,
      ~server_connected,
      ~callbacks,
      ~items,
      ~state,
      _children,
    ) => {
  ...component,

  render: _ =>
    <div className="columns">
      <div className="column is-6 message is-primary">

          <div className="message-header">
            {("Source [id " ++ id->string_of_int ++ "]")->ReasonReact.string}
            <Dropdown.Jsx2 items active_item="presets" />
          </div>
          <div>
            <MonacoEditor
              height=300
              width="100%"
              language="michelson"
              theme="gruvbox"
              value={state.text}
              onChange={(txt, _) => callbacks.update_text(txt)}
              // We use bs.raw for now, but this could be a record
              options=[%bs.raw
                {|{ automaticLayout: true, minimap: {enabled: false}, hover : {enabled: true}}|}
              ]
              editorDidMount={(editor, monaco) => {
                let decorate = setDecoration(editor, monaco);
                callbacks.load_decorate(decorate);
                if (init_monaco^) {
                  init_monaco := false;
                  let f =
                    (. _text, word, _word_start, _word_stop, _line, _col) =>
                      {switch (
                         List.find(
                           (prim: Doc.prim) => prim.label === word,
                           Doc.prims,
                         )
                       ) {
                       | exception _ => ""
                       | prim => prim.doc
                       }};
                  set_hover(editor, monaco, f);
                  set_syntax_highlighting(monaco);
                  set_completion(monaco);
                  callbacks.load_editor(monaco);
                };
              on_cursor_move(editor, callbacks.update_typemap);
              }}
            />
            <p> {("Address: " ++ state.address)->React.string} </p>
            <p> "Storage:"->React.string </p>
            <div className="field">
              <p className="control has-icons-left">
                <input
                  className="input"
                  value={state.storage}
                  onChange={e =>
                    callbacks.update_storage(e->ReactEvent.Form.target##value)
                  }
                />
                <span className="icon is-left">
                  <i className="fas fa-archive" />
                </span>
              </p>
            </div>
            <div className="field">
              <p className="control has-icons-left">
                <input
                  className="input"
                  value={state.balance}
                  onChange={e =>
                    callbacks.update_balance(e->ReactEvent.Form.target##value)
                  }
                />
                <span className="icon is-left">
                  {js|µꜩ|js}->ReasonReact.string
                </span>
              </p>
            </div>
            <button
              className="button"
              onClick={_ => {Js.log("hello"); callbacks.gen_storage(state.text)}}>
              "Generate storage"->ReasonReact.string
            </button>
            <button
              className="button"
              onClick={_ => callbacks.delete_me()}
              disabled={is_tracing || is_alone}>
              <span className="file-icon">
                <i className="fas fa-trash-alt"/>
              </span>
              "Delete me"->ReasonReact.string
            </button>
            <button
              className="button"
              disabled={!server_connected || is_tracing}
              onClick={_ => {
                let code = state.text;
                ignore(
                  Request.Request.indent(~code)
                  >>- Json.Decode.string
                  >>- (code => callbacks.update_text(code)),
                );
              }}>
              "Indent (but lose comments)"->ReasonReact.string
            </button>
            <div className="file">
              <label className="file-label">
                <input
                  type_="file"
                  className="file-input"
                  id="file-choose"
                  onChange={_ =>
                    Utils.Web.read_file("file-choose", str =>
                      callbacks.update_text(str)
                    )
                  }
                />
                <span className="file-cta">
                  <span className="file-icon">
                    <i className="fas fa-upload" />
                  </span>
                  <span className="file-label">
                    "Upload file"->ReasonReact.string
                  </span>
                </span>
              </label>
              <button
                className="button"
                onClick={_ => Utils.Web.save_file(state.text)}>
                <span className="file-icon">
                <i className="fas fa-download" />
                </span>
                "Save file"->ReasonReact.string
              </button>
              <button
                className="button"
                onClick={_ => Utils.Web.copy_to_clipboard(state.text)}>
                  <span className="file-icon">
                  <i className="fas fa-copy"/>
                  </span>
                  "Copy"->ReasonReact.string
              </button>
            </div>
          </div>
        </div>
        // <a href=
      <div className="column is-6 is-offset message is-info">
        <div className="message-header">
          <p> "State"->ReasonReact.string </p>
        </div>
        // "Bootstrap accounts: tz1KqTpEZ7Yob7QbPE4Hy4Wo8fHG8LhKxZSx"
        // ->ReasonReact.string
        {state.typechecks
           ? <p>
               "Typecheck "->ReasonReact.string
               <i className="fas fa-check green" />
             </p>
           : <p>
               "Typecheck "->ReasonReact.string
               <i className="fas fa-times red" />
             </p>}
        {switch (state.errors) {
         | None => ReasonReact.null
         | Some(err) =>
           <pre className="has-text-danger"> err.msg->ReasonReact.string </pre>
         }}
        {if (state.errors->Utils.Option.is_none && state.typechecks) {
           <p> "Ready to be deployed."->ReasonReact.string </p>;
         } else {
           ReasonReact.null;
         }}
        {switch (state.shown_typetrans) {
         | None => ReasonReact.null
         | Some({before, after}) =>
           let addbr = (i, s) =>
             <span>
               {(string_of_int(i) ++ ". " ++ s)->ReasonReact.string}
               <br />
             </span>;
           let stackify = l =>
             l |> List.mapi(addbr) |> Array.of_list |> ReasonReact.array;
           <p>
             <br />
             "before:"->ReasonReact.string
             <br />
             before->stackify
             <br />
             "after:"->ReasonReact.string
             <br />
             after->stackify
           </p>;
         }}
      </div>
    </div>,
};