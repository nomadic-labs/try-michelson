import { editor, languages } from "monaco-editor";
import { primitives } from './primitives'

export function get_value(editor: editor.IStandaloneCodeEditor) {
  let model = editor.getModel();
  return model.getValue();
}

export function onCursorMove(editor: editor.IStandaloneCodeEditor,
  range_callback: (line: number, col: number) => void) {
  editor.onDidChangeCursorPosition(e => {
    let line = e.position.lineNumber;
    let col = e.position.column;
    range_callback(line, col)
  })
}

export function setDecoration(editor: editor.IStandaloneCodeEditor,
  monaco: any) {
  monaco.languages.register({ id: "michelson" })
  var format = []
  let f = function (kind: string,
    loc: [number, number, number, number]) {
    let range = new monaco.Range(loc[0], loc[1], loc[2], loc[3]);
    let options = { inlineClassName: kind, hoverMessage: [{ value: "string" }] }
    var decs = [{ range: range, options: options }]
    format = editor.deltaDecorations(format, decs);
  }
  return f
}

export function setHover(editor: editor.ICodeEditor,
  monaco: any,
  hover_callback: (text: string, word: string, wordStart: number, wordStop: number, line: number, col: number) => string) {
  monaco.languages.registerHoverProvider("michelson", {
    provideHover: function (model: editor.ITextModel, position) {
      let word = model.getWordAtPosition(position);
      const range = new monaco.Range(position.lineNumber, word.startColumn, position.lineNumber, word.endColumn);
      let text = model.getLinesContent().join("\n");
      let str = hover_callback(text, word.word, word.startColumn, word.endColumn, position.lineNumber, position.column)
      return {
        range,
        contents: [{ value: str }],
      }
    }
  });
}

function type_completions () {
  return [{
    label: "int",
    insertText: "int",
  }, {label: "bool", insertText: "bool"}, {label : "unit", insertText: "unit" }]
}

export function setCompletion(monaco: any) {
monaco.languages.registerCompletionItemProvider('michelson', {
    provideCompletionItems: function(model, position) {
        // find out if we are completing a property in the 'dependencies' object.
        var textUntilPosition = model.getValueInRange({startLineNumber: 1, startColumn: 1, endLineNumber: position.lineNumber, endColumn: position.column});
        console.log(textUntilPosition)
        var match = textUntilPosition.match(/[{;]\s*\w+$/);
        var type_match = textUntilPosition.match(/[A-Z]+ [a-z]+$/)
        var suggestions = match ? primitives() : type_match ? type_completions() : [];
        return {
            suggestions: suggestions
        };
    }
})
}

 editor.defineTheme('gruvbox', {
    base: 'vs',
    inherit: false,
    rules: [
      {token: 'number', foreground: "#b16286"},
      {token: 'constant', foreground: '#b16286'},
      {token: 'string', foreground: '#b16286'},
      {token: 'primitive', foreground: '#458588'},
      {token: 'section', foreground: '#427b58'},
      {token: 'type', foreground: '#689d6a'},
      {token: 'comment', foreground: '#928374'},
    ],
    colors: {
      // ["editor.background"]: "#f9f5d7",
    'editor.foreground': 'gray',
    // 'editor.foreground': '#3c3836',
  }
    // colors: {}
  })


export function setSyntaxHighlighting(monaco: any) {
 languages.setMonarchTokensProvider('michelson', {
    tokenizer: {
      root: [
			[/[0-9]+/, "number"],
      [/[A-Z][A-Z]+/, "primitive"],
      [/[A-Z][a-z]+/, "constant"],
      [/\"[^\"]*\"/, "string"],
      [/#.*$/, "comment"],
      [/(code|parameter|storage)/, "section"],
      [/[a-z]+/, "type"],
      ]
    }
  });
  
}