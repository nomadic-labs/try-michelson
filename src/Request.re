open Fetch;
open PromiseMonad;
open Models;

type err = {msg: string};

module Decode = {
  open Json_decode;
  let error = json => {msg: json |> field("id", string)};
  let param_storage = json => {
    let t = (json |> field("parameter", string), json |> field("storage", string))
    Js.log("after decode");
    t
  }
};

module Encode = {
  open Json_encode;

  let typecheck_payload = (~code, ~storage, ~protocol) =>
    [("code", string(code)),
    ("storage", string(storage)),
    ("protocol", string(Protocol.to_string(protocol)))]
    |> object_
  let contracts_payload = contracts =>
    contracts
    |> list(((addr, code, storage, balance)) =>
         [
           ("addr", string(addr)),
           ("code", string(code)),
           ("storage", string(storage)),
           ("balance", int(balance)),
         ]
         |> object_
       );

  let trace_payload =
      (
        ~code,
        ~storage,
        ~balance,
        ~parameter,
        ~gas_limit,
        ~amount,
        ~self,
        ~payer,
        ~entrypoint,
        ~source,
        ~contracts,
        ~boot_balances,
        ~timestamp,
        ~protocol,
      ) =>
    [
      ("code", string(code)),
      ("storage", string(storage)),
      ("balance", int(balance)),
      ("parameter", string(parameter)),
      ("gas", int(gas_limit)),
      ("amount", int(amount)),
      ("self", string(self)),
      ("entrypoint", string(entrypoint)),
      ("payer", string(payer)),
      ("source", string(source)),
      ("contracts", contracts_payload(contracts)),
      ("boot_balances", list(int, boot_balances)),
      ("timestamp", string(timestamp)),
      ("protocol", string(Protocol.to_string(protocol)))
    ]
    |> object_;
};

// This is hardcoded for now,
// but this ought to be functorized.
module Request = {
  type provider = {
    host: string,
    port: string,
  };

  [@bs.val]
  external provider_url: option(string) = "process.env.LANG_SERVER_URL";

  let root =
    switch (provider_url) {
    | Some(provider_url) => provider_url ++ "/"
    | None =>
      let provider = {host: "localhost", port: "8282"};
      "http://" ++ provider.host ++ ":" ++ provider.port ++ "/";
    };

  let hello = () => {
    fetch(root ++ "hello")
  };

  let gen_param_storage = (~code) => {
    fetchWithInit(
      root ++ "rand_param_storage",
      RequestInit.make(
        ~method_=Post,
        ~body=BodyInit.make(Js.Json.stringify(Js.Json.string(code))),
        ~headers=HeadersInit.make({"Content-Type": "application/json"}),
        ()
      )
    )
    >>= (json => {Js.log("fetched"); Response.json(json)})
  }

  let indent = (~code) => {
    fetchWithInit(
      root ++ "indent",
      RequestInit.make(
        ~method_=Post,
        ~body=BodyInit.make(Js.Json.stringify(Js.Json.string(code))),
        ~headers=HeadersInit.make({"Content-Type": "application/json"}),
        ()
      )
    )
    >>= Response.json
  }

  let expand = (~expr) => {
    fetchWithInit(
      root ++ "expand",
      RequestInit.make(
        ~method_=Post,
        ~body=BodyInit.make(Js.Json.stringify(Js.Json.string(expr))),
        ~headers=HeadersInit.make({"Content-Type": "application/json"}),
        (),
      ),
    )
    >>= Response.json;
  };

  let typecheck = (~code, ~storage, ~protocol) => {
    fetchWithInit(
      root ++ "typecheck",
      RequestInit.make(
        ~method_=Post,
        ~body=BodyInit.make(Js.Json.stringify(Encode.typecheck_payload(~code, ~storage, ~protocol))),
        ~headers=HeadersInit.make({"Content-Type": "application/json"}),
        (),
      ),
    )
    >>= Response.json
    >>- Utils.Log.pipe
  };

  let get_trace =
      (
        ~code,
        ~storage,
        ~balance,
        ~parameter,
        ~gas_limit,
        ~amount,
        ~self,
        ~payer,
        ~entrypoint,
        ~source,
        ~contracts: list((string, string, string, int)),
        ~boot_balances,
        ~timestamp,
        ~protocol,
      ) => {
    let payload =
      Encode.trace_payload(
        ~code,
        ~storage,
        ~balance,
        ~parameter,
        ~gas_limit,
        ~amount,
        ~self,
        ~payer,
        ~entrypoint,
        ~source,
        ~contracts,
        ~boot_balances,
        ~timestamp,
        ~protocol
      );
    fetchWithInit(
      root ++ "get_trace",
      RequestInit.make(
        ~method_=Post,
        ~body=BodyInit.make(Js.Json.stringify(payload)),
        ~headers=HeadersInit.make({"Content-Type": "application/json"}),
        (),
      ),
    )
    >>= Response.json
    >>- (
      json => {
        json;
      }
    );
  };
};