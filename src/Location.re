type point = {
  line: int,
  col: int,
  point: int,
};

type range = {
  start: point,
  stop: point,
};

type t =
  | Original(range)
  | Expanded(range);

let range = loc =>
  switch (loc) {
  | Original(range)
  | Expanded(range) => range
  };

module Lens = {
  let start = t => t->range.start;

  let stop = t => t->range.stop;

  let start_point = t => t->start.point;

  let stop_point = t => t->stop.point;

  let unpack = t =>
    (t->start.line, t->start.col, t->stop.line, t->stop.col)
};

let is_inside = (line, col, t) => {
  t->Lens.start.line <= line
  && t->Lens.stop.line >= line
  && t->Lens.start.col <= col
  && t->Lens.stop.col >= col;
};

let is_expanded =
  fun
  | Expanded(_) => true
  | Original(_) => false;

module Decode = {
  open Json_decode;
  let point = json => {
    line: json |> field("line", int),
    // FIXME: probably shouldn't do that here
    col: json |> field("column", int) |> (x => x + 1),
    point: json |> field("point", int),
  };

  let range = json => {
    start: json |> field("start", point),
    stop: json |> field("stop", point),
  };

  let t = json =>
    field("expanded", bool, json)
      ? Expanded(json |> field("location", range))
      : Original(json |> field("location", range));
};

module Repr = {
  let range = range =>
    string_of_int(range.start.point)
    ++ "->"
    ++ string_of_int(range.stop.point);

  let t = location =>
    switch (location) {
    | Original(r) => "O [" ++ range(r) ++ "]"
    | Expanded(r) => "E [" ++ range(r) ++ "]"
    };
};