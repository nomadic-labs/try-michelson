module ParamsForm = {
  open Formality;

  type field =
    | Parameter
    | GasLimit
    | Timestamp
    | Amount
    | Payer
    | Entrypoint
    | Id
    | One
    | Two
    | Three
    | Four;

  type state = {
    parameter: string,
    gas_limit: string,
    amount: string,
    id: string,
    timestamp: string,
    payer: string,
    id_exists: ref(int => bool),
    one: string,
    two: string,
    three: string,
    four: string,
    entrypoint: string,
  };

  type message = string;
  type submissionError = unit;

  module ParameterField = {
    let update = (state, value) => {...state, parameter: value};

    let validator = {
      field: Parameter,
      strategy: Strategy.OnSubmit,
      dependents: None,
      validate: state =>
        switch (state.parameter) {
        | "" => Error("empty param")
        | _ => Ok(Valid)
        },
    };
  };

  module TimestampField = {
    let update = (state, value) => {...state, timestamp: value};

    let validator = {
      field: Timestamp,
      strategy: Strategy.OnFirstChange,
      dependents: None,
      validate: state =>
        switch (state.timestamp) {
        | ""
        | "now" => Ok(Valid)
        | s => switch (Utils.Date.parse_opt(s)) {
          | None => Error("not a date nor now")
          | Some(d) => {Js.log(d->Utils.Date.toString); Ok(Valid)}
        }
        },
    };
  };

  module GasLimitField = {
    let update = (state, value) => {...state, gas_limit: value};

    let validator = {
      field: Parameter,
      strategy: Strategy.OnFirstChange,
      dependents: None,
      validate: state =>
        switch (state.gas_limit) {
        | "" => Error("empty gas limit")
        | s when Utils.is_int(s) => 
          if (int_of_string(s) < 8_000_000) {
            Ok(Valid)
          } else {
            Error("Gas superior to hard limit")
          }
        | _ => Error("Not an int")
        },
    };
  };

  module AmountField = {
    let update = (state, value) => {...state, amount: value};

    let validator = {
      field: Amount,
      strategy: Strategy.OnSubmit,
      dependents: None,
      validate: state =>
        switch (state.amount) {
        | "" => Error("Empty amount")
        | s when Utils.is_int(s) => Ok(Valid)
        | _ => Error("Not an int")
        },
    };
  };

  module EntrypointField = {
    let update = (state, value) => {...state, entrypoint: value}

    let validator = {
      field:Entrypoint,
      strategy: Strategy.OnSubmit,
      dependents: None,
      validate: _state =>
        Ok(Valid)
    }
  }

  module IdField = {
    let update = (state, value) => {...state, id: value};

    let validator = {
      field: Id,
      strategy: Strategy.OnSubmit,
      dependents: None,
      validate: state => {
        switch (state.id) {
        | "" => Error("empty")
        | id when Utils.is_int(id) =>
          let id = int_of_string(id);
          if (state.id_exists^(id)) {
            Ok(Valid);
          } else {
            Error("id not found");
          };
        | _ => Error("not an id")
        };
      },
    };
  };

  module PayerField = {
    let update = (state, value) => {...state, payer: value};

    let validator = {
      field: Payer,
      strategy: Strategy.OnSubmit,
      dependents: None,
      validate: state => {
        switch (state.payer) {
        | "" => Error("Empty field")
        | payer =>
          if (Array.to_list(Genesis.bootstraps) |> List.mem(payer)) {
            Ok(Valid);
          } else {
            Error("Not a bootstrap address");
          }
        };
      },
    };
  };

  module OneField = {
    let update = (state, value) => {...state, one: value};
    let validator = {
      field: One,
      strategy: Strategy.OnFirstBlur,
      dependents: None,
      validate: state =>
        Utils.is_int(state.one) ? Ok(Valid) : Error("not an int"),
    };
  };
  module TwoField = {
    let update = (state, value) => {...state, two: value};
    let validator = {
      field: Two,
      strategy: Strategy.OnFirstBlur,
      dependents: None,
      validate: state =>
        Utils.is_int(state.two) ? Ok(Valid) : Error("not an int"),
    };
  };
  module ThreeField = {
    let update = (state, value) => {...state, three: value};
    let validator = {
      field: Three,
      strategy: Strategy.OnFirstBlur,
      dependents: None,
      validate: state =>
        Utils.is_int(state.three) ? Ok(Valid) : Error("not an int"),
    };
  };
  module FourField = {
    let update = (state, value) => {...state, four: value};
    let validator = {
      field: Four,
      strategy: Strategy.OnFirstBlur,
      dependents: None,
      validate: state =>
        Utils.is_int(state.four) ? Ok(Valid) : Error("not an int"),
    };
  };

  let validators = [
    ParameterField.validator,
    GasLimitField.validator,
    AmountField.validator,
    IdField.validator,
    PayerField.validator,
    TimestampField.validator,
    EntrypointField.validator,
    OneField.validator,
    TwoField.validator,
    ThreeField.validator,
    FourField.validator,
  ];
  module Repr = {};
};

module ParamsFormHook = Formality.Make(ParamsForm);

module Bootstraps = {
  open ParamsForm;
  let get_list = state =>
    [state.one, state.two, state.three, state.four]
    |> List.map(int_of_string);
  let to_string =
    fun
    | One => "one"
    | Two => "two"
    | Three => "three"
    | Four => "four"
    | _ => "default";

  let view = state =>
    fun
    | One => state.one
    | Two => state.two
    | Three => state.three
    | Four => state.four
    | _ => assert(false);

  let update = state => {
    switch (state) {
    | One => OneField.update
    | Two => TwoField.update
    | Three => ThreeField.update
    | Four => FourField.update
    | _ => assert(false)
    };
  };

  let address =
    fun
    | One => Genesis.bootstraps[0]
    | Two => Genesis.bootstraps[1]
    | Three => Genesis.bootstraps[2]
    | Four => Genesis.bootstraps[3]
    | _ => assert(false);

  let html = (form: ParamsFormHook.interface, variant) => {
    <div className="field no-margin">
      <div className="field">
        <div className="columns">
          <div className="column is-7">
            {address(variant)->ReasonReact.string}
          </div>
          <div className="column is-5">
            <p className="control has-icons-left">
              <input
                className="input"
                placeholder={"balance of bootstrap " ++ variant->to_string}
                value={form.state->view(variant)}
                disabled={form.submitting}
                onBlur={_ => form.blur(variant)}
                onChange={event =>
                  form.change(
                    variant,
                    variant->update(
                      form.state,
                      event->ReactEvent.Form.target##value,
                    ),
                  )
                }
              />
              <span className="icon is-left">
                {js|µꜩ|js}->ReasonReact.string
              </span>
            </p>
          </div>
        </div>
      </div>
      {switch (form.result(variant)) {
       | Some(Error(msg)) =>
         <div className="form-message failure"> msg->React.string </div>
       | Some(Ok(Valid | NoValue))
       | None => React.null
       }}
    </div>;
  };
};

[@react.component]
let make =
    (
      ~submit,
      ~parameter,
      ~gas_limit,
      ~amount,
      ~id,
      ~id_exists,
      ~server_connected,
      ~payer,
      ~balances,
      ~contracts,
    ) => {
  let initialState =
    ParamsForm.{
      parameter,
      gas_limit,
      amount,
      id,
      id_exists,
      timestamp: [%bs.raw {| new Date().toISOString() |}],
      payer,
      one: balances[0],
      two: balances[1],
      three: balances[2],
      four: balances[3],
      entrypoint: "default"
    };
  let form =
    ParamsFormHook.useForm(
      ~initialState,
      ~onSubmit=(state, form) => {
        // int of strings should be safe as the data has been validated
        submit(
          state.parameter,
          int_of_string(state.gas_limit),
          int_of_string(state.amount),
          int_of_string(state.id),
          state.payer,
          state.entrypoint,
          Bootstraps.get_list(state),
        );
        form.notifyOnSuccess(Some(state));
      },
    );
  <section className="section">
    <div className="container box">
      <form
        className="form" onSubmit={form.submit->Formality.Dom.preventDefault}>
        <div className="columns">
          <div className="column is-5">
            /* Params */

              <div className="field">
                <p className="control has-icons-left">
                  <input
                    className="input"
                    placeholder="Parameter"
                    value={form.state.parameter}
                    disabled={form.submitting}
                    onBlur={_ => form.blur(Parameter)}
                    onChange={event =>
                      form.change(
                        Parameter,
                        ParamsForm.ParameterField.update(
                          form.state,
                          event->ReactEvent.Form.target##value,
                        ),
                      )
                    }
                  />
                  <span className="icon is-left">
                    <i className="fas fa-file-import" />
                  </span>
                </p>
              </div>
              {switch (form.result(Parameter)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   //  {Cn.make(["form-message", "failure"])}>
                    msg->React.string </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}
              /* Gas Limit */
              <div className="field">
                <p className="control has-icons-left">
                  <input
                    className="input"
                    placeholder="Gas limit"
                    value={form.state.gas_limit}
                    disabled={form.submitting}
                    onBlur={_ => form.blur(GasLimit)}
                    onChange={event =>
                      form.change(
                        GasLimit,
                        ParamsForm.GasLimitField.update(
                          form.state,
                          event->ReactEvent.Form.target##value,
                        ),
                      )
                    }
                  />
                  <span className="icon is-left">
                    <i className="fas fa-gas-pump" />
                  </span>
                </p>
              </div>
              {switch (form.result(GasLimit)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   //  {Cn.make(["form-message", "failure"])}>
                    msg->React.string </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}
              /* Amount */
              <div className="field">
                <p className="control has-icons-left">
                  <input
                    className="input"
                    placeholder="Amount"
                    value={form.state.amount}
                    disabled={form.submitting}
                    onBlur={_ => form.blur(Amount)}
                    onChange={event =>
                      form.change(
                        Amount,
                        ParamsForm.AmountField.update(
                          form.state,
                          event->ReactEvent.Form.target##value,
                        ),
                      )
                    }
                  />
                  <span className="icon is-left">
                    {js|µꜩ|js}->ReasonReact.string
                  </span>
                </p>
              </div>
              {switch (form.result(Amount)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   //  {Cn.make(["form-message", "failure"])}>
                    msg->React.string </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}
              /* Contract to call */
              <div className="field">
                <p className="control has-icons-left">
                  <div className="select">
                    <select onChange={event =>
                      form.change(
                        Id,
                        ParamsForm.IdField.update(
                          form.state,
                          event->ReactEvent.Form.target##value
                        ),
                      )}>
                      {Js.log(contracts); contracts
                        |> List.map(((id, _)) =>
                          <option> id->string_of_int->ReasonReact.string </option>)
                        |> Array.of_list
                        |> ReasonReact.array }
                    </select>
                  </div>
                  <span className="icon is-left">
                    "id"->ReasonReact.string
                  </span>
                </p>
              </div>
              {switch (form.result(Id)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   msg->React.string
                 </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}
              /* Payer */
              <div className="field">
                <p className="control has-icons-left">
                <div className="select">
                  <select>
                    <option selected=true>Genesis.bootstraps[0]->ReasonReact.string</option>
                    <option >Genesis.bootstraps[1]->ReasonReact.string</option>
                    <option >Genesis.bootstraps[2]->ReasonReact.string</option>
                    <option >Genesis.bootstraps[3]->ReasonReact.string</option>
                  </select>
                </div>
                  <span className="icon is-left">
                    <i className="fas fa-address-card" />
                  </span>
                </p>
              </div>
              {switch (form.result(Payer)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   msg->React.string
                 </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}

              <div className="field">
                <p className="control has-icons-left">
                  <div className="select">
                    <select onChange={event =>
                      form.change(
                        Entrypoint,
                        ParamsForm.EntrypointField.update(
                          form.state,
                          event->ReactEvent.Form.target##value
                        ),
                      )}>
                      {contracts
                        |> List.assoc(form.state.id->int_of_string)
                        |> (contract => contract.Models.Contract.entrypoints)
                        |> List.map(entrypoint =>
                          <option>entrypoint->ReasonReact.string</option>)
                        |> Array.of_list
                        |> ReasonReact.array
                      }
                    </select>
                  </div>
                  <span className="icon is-left">
                    <i className="fas fa-stream"/>
                  </span>
                </p>
              </div>
              <div className="field">
                <p className="control has-icons-left">
                  <input
                    className="input"
                    placeholder="timestamp"
                    value={form.state.timestamp}
                    disabled={form.submitting}
                    onBlur={_ => form.blur(Timestamp)}
                    onChange={event =>
                      form.change(
                        Timestamp,
                        ParamsForm.TimestampField.update(
                          form.state,
                          event->ReactEvent.Form.target##value,
                        ),
                      )
                    }
                  />
                  <span className="icon is-left">
                    <i className="fas fa-clock" />
                  </span>
                </p>
              </div>
              {switch (form.result(Timestamp)) {
               | Some(Error(msg)) =>
                 <div className="form-message failure">
                   msg->React.string
                 </div>
               | Some(Ok(Valid | NoValue))
               | None => React.null
               }}
            </div>
          <div className="column is-half">
            <h3 className="subtitle has-text-centered">
              "Bootstrap accounts"->ReasonReact.string
            </h3>
            {Bootstraps.html(form, One)}
            {Bootstraps.html(form, Two)}
            {Bootstraps.html(form, Three)}
            {Bootstraps.html(form, Four)}
          </div>
        </div>
        <button
          className="button" disabled={form.submitting || !server_connected}>
          (form.submitting ? "Submitting..." : "Run Transaction")->React.string
        </button>
      </form>
    </div>
  </section>;
};

module Jsx2 = {
  let component = ReasonReact.statelessComponent("ParamsForms");

  let make =
      (
        ~submit,
        ~parameter,
        ~gas_limit,
        ~amount,
        ~id,
        ~id_exists,
        ~server_connected,
        ~payer,
        ~balances,
        ~contracts,
        // ~key,
      ) =>
    ReasonReactCompat.wrapReactForReasonReact(
      make,
      makeProps(
        ~submit,
        ~parameter,
        ~gas_limit,
        ~amount,
        ~id,
        ~id_exists,
        ~server_connected,
        ~payer,
        ~balances,
        ~contracts,
        // ~key,
        (),
      ),
    );
};