[@bs.config {jsx: 3}];
type action =
  | Switch;

type state = {
  content: ReasonReact.reactElement,
  hidden: ReasonReact.reactElement,
  dropped: bool,
};

// let component = ReasonReact.reducerComponent("Dropcard");

[@react.component]
let make = (~content, ~hidden) => {
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | Switch => {...state, dropped: !state.dropped}
        },
      {content, hidden, dropped: false},
    );

  <div className="card">
    <header className="card-header">
      content
      <a className="card-header-icon" onClick={_ => dispatch(Switch)}>
        <span className="icon">
          {state.dropped
             ? <i className="fas fa-angle-up" ariaHidden=true />
             : <i className="fas fa-angle-down" ariaHidden=true />}
        </span>
      </a>
    </header>
    {state.dropped
       ? <div className="card-content">
           <div className="content"> {state.hidden} </div>
         </div>
       : ReasonReact.null}
  </div>;
} /* */;

module Jsx2 = {
  let component = ReasonReact.statelessComponent("Dropcard");
  let make = (~content, ~hidden) =>
    ReasonReactCompat.wrapReactForReasonReact(
      make,
      makeProps(~content, ~hidden, ())
    )
}