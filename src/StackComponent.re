open Utils;
open BsRecharts;
open Models;

let bar_of_item = (index, _item) => {
  <Bar
    dataKey="value"
    fill="#2078b4"
    stackId="best_stack"
    isAnimationActive=false>
    <LabelList dataKey={string_of_int(index)} />
  </Bar>;
};

let pad = (max_width, s) => {
  let len = String.length(s);
  if (len > max_width) {
    String.sub(s, 0, max_width - 3) ++ "...";
  } else {
    let space = (max_width - len) / 2;
    String.make(space, ' ') ++ s ++ String.make(space + 1, ' ');
  };
};

// let component = ReasonReact.reducerComponent("StackComponent");
let component = ReasonReact.statelessComponent("StackComponent");


let make = (~stack: Stack.t, ~max_stack_size: int, _children) => {
  ...component,

  render: _self => {
    let max_chars = 32;
    let stack = List.rev(stack);
    let pad_stack = List.init(max_stack_size - List.length(stack), "");
    let stack = stack @ pad_stack;
    List.iter(
      item => {
        print_string(item);
        print_string(" | ");
      },
      stack,
    );
    flush_all();
    let stack = List.map(pad(max_chars), stack);
    let data = Stack.to_data(stack);
    let len = List.length(stack);
    // let width =
    //   stack
    //     |> List.map(String.length)
    //     |> List.fold_left((acc: int, size: int) => (size > acc) ? size : acc, 0)
    //     |> (max => float_of_int(max * 13))
    let width = float_of_int(max_chars * 13);
    let height = float_of_int(len * 50);
    <div style={ReactDOMRe.Style.make(~float="left", ())}>
      <ResponsiveContainer height={Px(height)} width={Px(width)}>
        <BarChart data> {List.mapi(bar_of_item, stack)} </BarChart>
      </ResponsiveContainer>
    </div>;
  },
};