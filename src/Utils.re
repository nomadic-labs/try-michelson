module List = {
  include List;
  let filter_opt = l => {
    let rec filter_opt = l =>
      fun
      | [] => l
      | [Some(x), ...tl] => filter_opt([x, ...l], tl)
      | [None, ...tl] => filter_opt(l, tl);

    List.rev(filter_opt([], l));
  };

  let take = (n, l) =>
    l |> List.mapi((i, elt) => i >= n ? None : Some(elt)) |> filter_opt;

  let sub = (from, until, l) =>
    l
    |> List.mapi((i, elt) => i < from || i >= until ? None : Some(elt))
    |> filter_opt;

  let take_from_last = (n, l) => {
    let until = List.length(l);
    let from = until - n;
    sub(from, until, l);
  };

  let take_from = (n, l) => sub(n, List.length(l), l);

  let init = (len, x) => {
    let rec init_rec = (len, x, out) =>
      switch (len) {
      | 0 => List.rev(out)
      | len => init_rec(len - 1, x, [x, ...out])
      };
    init_rec(len, x, []);
  };
};

module Option = {
  let is_none =
    fun
    | Some(_) => false
    | None => true;

  let is_some =
    fun
    | None => false
    | Some(_) => true;
};

module Log = {
  let pipe = x => {
    Js.log(x);
    x;
  };
};

module Web = {
  let read_file =
    /* We can't do this in a typesafe way cause FileReader is unimplemented in
       bs-webapi */
    [%bs.raw
      {|
    function (id, callback) {
    let file = document.getElementById(id).files[0]
    var reader = new FileReader();
    reader.readAsText(file, "UTF-8");
    reader.onload = function (evt) {
        callback(evt.target.result)
        console.log("ok")
    };
    reader.onerror = function (evt) {
      console.log("error")
    }
    }
  |}
    ];

  let save_file = [%bs.raw
    {|
    function (content) {
      let file = new Blob([content], {type: "text/plain"})
      var a = document.createElement("a");
      let url = URL.createObjectURL(file);
      a.href = url;
      a.download = "contract.tz";
      document.body.appendChild(a);
      a.click();
      setTimeout(function() {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0)
    }
    |}
  ];

  let copy_to_clipboard = [%bs.raw
    {|
    function(text) {
      var a = document.createElement("textarea")
      a.style.position = "fixed";
      a.style.top = 0;
      a.style.left = 0;
      a.style.width = '2em';
      a.style.height = '2em';
      a.style.padding = 0
      a.style.border = 'none';
      a.style.outline = 'none';
      a.style.boxShadow = 'none';
      a.style.background = 'transparent';
      a.value = text;
      document.body.appendChild(a);
      a.focus();
      a.select();
      document.execCommand('copy')
      setTimeout(function() {
        document.body.removeChild(a)
      })
    }
  |}
  ];
};

let is_int = str =>
  switch (int_of_string(str)) {
  | exception _ => false
  | _ => true
  };

module Date = {
  type t;
  type x;
  [@bs.new] external newDate: string => t = "Date";
  [@bs.send] external toString: t => string = "toString";

  let parse = str => newDate(str);

  let parse_opt = str => {
    let date = newDate(str);
    switch (date->toString) {
    | "Invalid Date" => None
    | _ => Some(date)
    };
  };
};

module Url = {
  let url = [%bs.raw {| location.search |}];

  let params = [%bs.raw {| new URLSearchParams(url) |}];
  let get_params = _url => [%bs.raw {| new URLSearchParams(_url)|}];
  let has = (_label, _params) => [%bs.raw
    {| ((label, _params) => params.has(label))(_label, _params)|}
  ];

  let to_string = _params => [%bs.raw
    {| ((params) => params.toString())(_params) |}
  ];

  let get_with_default = (_label, _def, _params) => {
    %bs.raw
    {|
    ((label, def, params) => {
    if (params.has(label)) {
      return params.get(label);
    } else {
      return def;
    }})(_label, _def, _params)
  |};
  };
};