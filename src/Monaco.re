type monaco;
type editor;

[@bs.module "./monacot"]
external get_value: editor => string = "get_value";

[@bs.module "./monacot"]
external on_cursor_move: (editor, (. int, int) => unit) => unit = "onCursorMove";

[@bs.module "./monacot"]
external setDecoration:
  (editor, monaco) => (. string, (int, int, int, int)) => unit =
  "setDecoration";
[@bs.module "./monacot"]
external set_syntax_highlighting:
  monaco => unit = "setSyntaxHighlighting";

[@bs.module "./monacot"]
external set_hover:
  (
    editor,
    monaco,
    (. string, string, int, int, int, int) => string
  ) =>
  unit =
  "setHover";
[@bs.module "./monacot"]
external set_completion: monaco => unit = "setCompletion";
