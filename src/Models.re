module Protocol = {
  type t =
    | Athens
    | Babylon;

  let to_string =
    fun
    | Athens => "athens"
    | Babylon => "babylon";

  let of_string_exn =
    fun
    | "athens" => Athens
    | "babylon" => Babylon
    | _ => failwith("not a known protocol");

  module Decode = {
    open Json.Decode;

    let t = json => json |> string |> of_string_exn;
  };
};

module Address = {
  type t = string;

  type kind =
    | Account
    | Contract
    | Unknown;

  let is_account = t => String.sub(t, 0, 3) === "tz1";

  let is_contract = t => String.sub(t, 0, 3) === "KT1";

  let kind = t =>
    if (is_account(t)) {
      Account;
    } else if (is_contract(t)) {
      Contract;
    } else {
      Unknown;
    };

  module Repr = {
    let shortened = addr =>
      String.sub(addr, 0, 7) ++ "..." ++ String.sub(addr, 33, 3);
  };
};

module Typemap = {
  type trans = {
    loc: Location.t,
    before: list(string),
    after: list(string),
  };

  type t = list(trans);

  let at = (line, col, t) => {
    switch (
      List.find(
        ({loc}) => Location.is_inside(line, col, loc),
        List.rev(t),
      )
    ) {
    | trans => Some(trans)
    | exception _ => None
    };
  };

  let filter_expanded = t =>
    List.filter(({loc}) => !Location.is_expanded(loc), t);

  module Decode = {
    open Json_decode;

    let trans = json => {
      loc: json |> field("location", Location.Decode.t),
      before: json |> field("before", list(string)),
      after: json |> field("after", list(string)),
    };

    let t = json => json |> list(trans);
  };
};

module Entrypoint = {
  type t = {
    name: string,
    ty: string,
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      name: json |> field("name", string),
      ty: json |> field("ty", string),
    };
  };
};

module TypecheckResponse = {
  type t = {
    typemap: Typemap.t,
    entrypoints: list(Entrypoint.t),
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      typemap: json |> field("typemap", Typemap.Decode.t),
      entrypoints: json |> field("entrypoints", list(Entrypoint.Decode.t)),
    };
  };
};

module Error = {
  type t =
    | BadStackType(string, Location.t)
    | Generic(string, Location.t)
    | Simple(string);

  type typecheck_error = {
    msg: string,
    details: list(t),
    typemap: Typemap.t,
  };

  let get_loc_opt =
    fun
    | BadStackType(_, loc)
    | Generic(_, loc) => Some(loc)
    | _ => None;

  module Decode = {
    open Json_decode;

    let generic = json =>
      Generic(
        json |> field("id", string),
        json |> field("location", Location.Decode.t),
      );

    let bad_stack_type = json =>
      BadStackType(
        json |> field("id", string),
        json |> field("location", Location.Decode.t),
      );

    /* FIXME: We shouldn't need that hack */
    let simple = json => Simple(json |> field("id", string));

    let either = oneOf([bad_stack_type, generic, simple]);
    let list = list(either);

    let typecheck_error = json => {
      msg: json |> field("msg", string),
      details: json |> field("errors", list),
      typemap: json |> field("typemap", Typemap.Decode.t),
    };
  };
};

module Contract = {
  type t = {
    editor: ref(option(Monaco.monaco)),
    decorate: ref((. string, (int, int, int, int)) => unit),
    text: string,
    address: string,
    balance: string,
    storage: string,
    typechecks: bool,
    entrypoints: list(string),
    errors: option(Error.typecheck_error),
    typemap: Typemap.t,
    shown_typetrans: option(Typemap.trans),
    timers: list(Js.Global.timeoutId),
  };

  type callbacks = {
    update_text: string => unit,
    update_storage: string => unit,
    update_balance: string => unit,
    load_editor: Monaco.monaco => unit,
    load_decorate: ((. string, (int, int, int, int)) => unit) => unit,
    delete_me: unit => unit,
    update_typemap: (. int, int) => unit,
    gen_storage: string => unit,
  };

  let make = (code, storage, balance, addr) => {
    editor: ref(None),
    decorate: ref((. _, _) => ()),
    text: code,
    address: addr,
    balance,
    storage,
    errors: None,
    entrypoints: ["default"],
    typemap: [],
    typechecks: false,
    timers: [],
    shown_typetrans: None,
  };

  let load_editor = (contract, monaco) => contract.editor := Some(monaco);

  let load_decorate = (contract, f) => contract.decorate := f;

  let decorate = (contract, kind, loc) => {
    contract.decorate^(. kind, loc->Location.Lens.unpack);
  };

  let undecorate = contract => {
    contract.decorate^(. "", (0, 0, 0, 0));
  };
};

module Stack = {
  type item = string;
  type t = list(item);

  let iter = (obj, index, item) => {
    let key = string_of_int(index);
    let value = item;
    Js.Dict.set(obj, key, value);
  };

  let to_data = stack => {
    let data = Js.Dict.empty();
    List.iteri(iter(data), stack);

    Js.Dict.set(data, "value", [%bs.raw {|1|}]);
    [|data|];
  };
};

module Trace = {
  type state = {
    location: Location.t,
    stack: Stack.t,
    gas: int,
  };
  type t = list(state);

  let max_stack_size = (t: t) =>
    List.map(s => List.length(s.stack), t)
    |> List.fold_left((max, len) => max < len ? len : max, 0)
    |> Utils.Log.pipe;

  let sort = (l: t) => {
    let cmp = (a, b) =>
      if (Location.Lens.start_point(a.location)
          < Location.Lens.start_point(b.location)) {
        (-1);
      } else if (Location.Lens.start_point(a.location)
                 === Location.Lens.start_point(b.location)) {
        0;
      } else {
        1;
      };
    List.sort(cmp, l);
  };

  module Decode = {
    open Json_decode;
    let item = json => {
      json |> field("item", string);
    };

    let state = json => {
      location: json |> field("location", Location.Decode.t),
      stack: json |> field("stack", list(item)),
      gas: json |> field("gas", string) |> int_of_string,
    };

    let t = list(state);
  };
};

module Parameter = {
  type t = {
    entrypoint: string,
    value: string,
  };

  module Repr = {
    let to_string = t => t.value ++ " @" ++ t.entrypoint;
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      entrypoint: json |> field("entrypoint", string),
      value: json |> field("value", string),
    };
  };
};

module Transaction = {
  type t = {
    amount: int,
    destination: string,
    parameters: Parameter.t,
    source: string,
  };

  let is_transfer = t => Address.is_account(t.destination);

  let is_call = t => Address.is_contract(t.destination);

  module Repr = {
    let kind =
      fun
      | Address.Account => "[TRANSFER]"
      | Address.Contract => "[CALL]"
      | Address.Unknown => "[UNKNOWN]";

    let to_string = t =>
      t.destination->Address.kind->kind
      ++ " "
      ++ t.source
      ++ {js| → |js}
      ++ t.destination
      ++ " [ "
      ++ string_of_int(t.amount)
      ++ {js|ꜩ | |js}
      ++ t.parameters->Parameter.Repr.to_string
      ++ "]";

    let abstract = t =>
      " "
      ++ t.source->Address.Repr.shortened
      ++ {js| → |js}
      ++ t.destination->Address.Repr.shortened
      ++ " "
      ++ string_of_int(t.amount)
      ++ {js|ꜩ |js};
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      amount: json |> field("amount", string) |> int_of_string,
      destination: json |> field("destination", string),
      source: json |> field("source", string),
      parameters:
        json
        |> Json.Decode.oneOf([
             field("parameters", Parameter.Decode.t),
             // The server doesn't return a parameter field if no
             // entrypoint is specified and the value is of type unit
             _ => {entrypoint: "default", value: "Unit"},
           ]),
    };
  };
};

module Origination = {
  type t = {
    balance: string,
    delegatable: bool,
    manager_pubkey: string,
    source: string,
    spendable: option(bool),
  };

  module Repr = {
    let to_string = t => "[ORIGINATION] from " ++ t.source;

    let abstract = to_string;
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      balance: json |> field("balance", string),
      delegatable: json |> field("delegatable", bool),
      manager_pubkey: json |> field("manager_pubkey", string),
      source: json |> field("source", string),
      spendable: json |> optional(field("spendable", bool)),
    };
  };
};

module Delegation = {
  type t = {
    delegate: string,
    source: string,
  };

  module Repr = {
    let to_string = t => {
      t.source ++ " delegates to " ++ t.delegate;
    };

    let abstract = t => {
      "[DELEGATION] "
      ++ t.source->Address.Repr.shortened
      ++ {js| → |js}
      ++ t.delegate->Address.Repr.shortened;
    };
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      delegate: json |> field("delegate", string),
      source: json |> field("source", string),
    };
  };
};

/* module Status = {

     module Decode = {
       open Json.Decode;
    };
   }; */

module BalanceUpdate = {
  type t = {
    change: string,
    contract: string,
  };

  module Decode = {
    open Json.Decode;
    let t = json => {
      change:
        json
        |> field("change", string)
        |> (s => String.sub(s, 0, 1) === "-" ? s : "+" ++ s),
      contract: json |> field("contract", string),
    };
  };

  module Repr = {
    let to_string = t => t.contract ++ " " ++ t.change;
  };
};

module OperationResult = {
  type transaction_result = {
    trace: option(Trace.t),
    balance_updates: list(BalanceUpdate.t),
    storage: option(string),
    paid_storage_size_diff: string,
    consumed_gas: string,
    storage_size: string,
  };

  type reveal_result = {consumed_gas: string};

  type origination_result = {
    balance_updates: list(BalanceUpdate.t),
    consumed_gas: string,
    storage_size: string,
    paid_storage_size_diff: string,
  };

  type delegation_result = {consumed_gas: string};

  type result =
    | TransactionResult(transaction_result)
    | DelegationResult(delegation_result)
    | OriginationResult(origination_result)
    | RevealResult(reveal_result);

  type status =
    | Ok(result)
    | Failed(string)
    | Skipped;

  type t = {status};

  let get_trace_opt = t =>
    switch (t.status) {
    | Ok(TransactionResult({trace})) => trace
    | _ => None
    };

  module Repr = {
    let transac_to_string = (res: transaction_result) => {
      let balance_updates =
        "balance updates:\n  "
        ++ (
          res.balance_updates
          |> List.map(BalanceUpdate.Repr.to_string)
          |> String.concat("\n  ")
        );
      let storage =
        switch (res.storage) {
        | None => ""
        | Some(storage) => "\n updated_storage: " ++ storage
        };
      balance_updates
      ++ storage
      ++ "\n storage_size: "
      ++ res.storage_size
      ++ " bytes\n paid_storage_size_diff: "
      ++ res.paid_storage_size_diff
      ++ " bytes\n consumed gas: "
      ++ res.consumed_gas;
    };

    let delegation_to_string = (res: delegation_result) =>
      " consumed_gas: " ++ res.consumed_gas;

    let reveal_to_string = (res: reveal_result) =>
      "\n consumed_gas: " ++ res.consumed_gas;

    let origination_to_string = (res: origination_result) => {
      let balance_updates =
        "balance updates:\n  "
        ++ (
          res.balance_updates
          |> List.map(BalanceUpdate.Repr.to_string)
          |> String.concat("\n  ")
        );
      "\n balance_updates: "
      ++ balance_updates
      ++ "\n consumed_gas: "
      ++ res.consumed_gas
      ++ "\n storage size: "
      ++ res.storage_size
      ++ "\n paid_storage_size_diff: "
      ++ res.paid_storage_size_diff;
    };

    let result_to_string =
      fun
      | TransactionResult(ok_res) => transac_to_string(ok_res)
      | DelegationResult(res) => delegation_to_string(res)
      | RevealResult(res) => reveal_to_string(res)
      | OriginationResult(res) => origination_to_string(res);

    let to_string = t =>
      switch (t.status) {
      | Failed(msg) => "Failed: " ++ msg
      | Skipped => "skipped"
      | Ok(result) => "ok:\n " ++ result_to_string(result)
      };

    let icon = t =>
      switch (t.status) {
      | Ok(_) => <i className="green fas fa-check" />
      | Failed(msg) =>
        <abbr title=msg> <i className="red fas fa-times" /> </abbr>
      | Skipped => <i className="fas fa-times" />
      };
  };

  module Decode = {
    open Json.Decode;

    let transaction = json => {
      trace: json |> optional(field("trace", Trace.Decode.t)),
      balance_updates:
        json |> field("balance_updates", list(BalanceUpdate.Decode.t)),
      storage: json |> optional(field("storage", string)),
      storage_size: json |> field("storage_size", string),
      paid_storage_size_diff: json |> field("paid_storage_size_diff", string),
      consumed_gas: json |> field("consumed_gas", string),
    };

    let reveal: Js.Json.t => reveal_result =
      json => {consumed_gas: json |> field("consumed_gas", string)};

    let delegation = json => {
      consumed_gas: json |> field("consumed_gas", string),
    };

    let origination = json => {
      balance_updates:
        json |> field("balance_updates", list(BalanceUpdate.Decode.t)),
      storage_size: json |> field("storage_size", string),
      paid_storage_size_diff: json |> field("paid_storage_size_diff", string),
      consumed_gas: json |> field("consumed_gas", string),
    };

    let result =
      oneOf([
        transaction |> map(t => TransactionResult(t)),
        delegation |> map(t => DelegationResult(t)),
        origination |> map(t => OriginationResult(t)),
      ]);

    let status = json =>
      switch (field("status", string, json)) {
      | "success" => Ok(json |> result)
      | "failed" => Failed(json |> field("msg", string))
      | "skipped" => Skipped
      | _ => assert(false)
      };
    let t = json => {status: json |> status};
  };
};

module Operation = {
  type t =
    | Transaction(Transaction.t)
    | Origination(Origination.t)
    | Delegation(Delegation.t);

  module Repr = {
    let to_string =
      fun
      | Transaction(t) => Transaction.Repr.to_string(t)
      | Origination(t) => Origination.Repr.to_string(t)
      | Delegation(t) => Delegation.Repr.to_string(t);

    let to_html =
      fun
      | Transaction(t) => Transaction.Repr.abstract(t)->ReasonReact.string
      | Origination(t) => Origination.Repr.abstract(t)->ReasonReact.string
      | Delegation(t) => Delegation.Repr.abstract(t)->ReasonReact.string;

    let abstract =
      fun
      | Transaction(t) => Transaction.Repr.abstract(t)->ReasonReact.string
      | Origination(t) => Origination.Repr.abstract(t)->ReasonReact.string
      | Delegation(t) => Delegation.Repr.abstract(t)->ReasonReact.string;
  };

  module Decode = {
    open Json.Decode;

    let t = json =>
      switch (json |> field("kind", string)) {
      | "transaction" => Transaction(Transaction.Decode.t(json))
      | "origination" => Origination(Origination.Decode.t(json))
      | "delegation" => Delegation(Delegation.Decode.t(json))
      | _ => assert(false)
      };
  };
};

module Receipt = {
  type t = {
    op: Operation.t,
    result: OperationResult.t,
  };

  module Repr = {
    let to_string = t =>
      Operation.Repr.to_string(t.op)
      ++ "["
      ++ OperationResult.Repr.to_string(t.result)
      ++ "]";

    let to_html = (t, show_trace_clbk, update_storage) => {
      let abstract = Operation.Repr.abstract(t.op);
      <Dropcard.Jsx2
        key={to_string(t)}
        content={
          <p className="card-header-title">
            abstract
            " "->ReasonReact.string
            {OperationResult.Repr.icon(t.result)}
            {switch (t.op, t.result) {
             | (
                 Transaction({destination, _}),
                 {status: Ok(TransactionResult({storage}))},
               )
                 when Address.is_contract(destination) =>
               <div>
                 <button className="button" onClick={_ => show_trace_clbk()}>
                   "Trace execution"->ReasonReact.string
                 </button>
                 {switch (storage) {
                  | None => ReasonReact.null
                  | Some(storage) =>
                    <button
                      className="button"
                      onClick={_ => update_storage(destination, storage)}>
                      "Update storage"->ReasonReact.string
                    </button>
                  }}
               </div>
             | _ => ReasonReact.null
             }}
          </p>
        }
        hidden={
          <pre>
            {Operation.Repr.to_string(t.op)->ReasonReact.string}
            <br />
            {OperationResult.Repr.to_string(t.result)->ReasonReact.string}
          </pre>
        }
      />;
    };
  };

  module Decode = {
    open Json.Decode;

    let t = json => {
      op: json |> field("op", Operation.Decode.t),
      result: json |> field("result", OperationResult.Decode.t),
    };
  };
};

module Snapshot = {
  type t = {receipt: Receipt.t};

  module Decode = {
    open Json.Decode;

    let t = json => {receipt: json |> field("receipt", Receipt.Decode.t)};
  };

  module Repr = {
    let dump_string = t =>
      "snapshot:\n  receipt: " ++ Receipt.Repr.to_string(t.receipt);
  };
};

module History = {
  type t = list(Snapshot.t);

  module Decode = {
    open Json.Decode;
    let t = json => json |> field("history", list(Snapshot.Decode.t));
  };
};
