# try-michelson

This is a widget/website to visualize the execution of smart contracts.

## Roadmap

* Interface change (bigger window for smart contracts)

## Run Project

This project is dependent on another project: [tezos-lang-server](https://gitlab.com/nomadic-labs/tezos-lang-server/).

It needs to be run for try-michelson to run.

```sh
yarn
yarn start
# in another tab
yarn server
```